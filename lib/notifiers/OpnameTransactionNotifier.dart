import 'package:flutter/cupertino.dart';
import 'package:jec_asset_mobile/data/services/APIProvider_AssetOpname.dart';
import 'package:jec_asset_mobile/models/commonModels/GetIDmodel.dart';
import 'package:jec_asset_mobile/models/commonModels/ModifiedResponseModel.dart';
import 'package:jec_asset_mobile/models/OpnameLIstModel.dart';

class AssetOpnameTransactionNotifier extends ChangeNotifier {
  List<String> barcodeList = [];
  bool bGetOpnameID = true;
  bool bFetchData = true;
  bool bClosedRoom = true;
  bool bUploadBarcode = true;
  bool bShowLoading = false;
  OpnameListModel listOpname = new OpnameListModel();
  ModifiedResponseModel closeRoomResponse = new ModifiedResponseModel();
  ModifiedResponseModel uploadOpname = new ModifiedResponseModel();
  GlobalModelGetId headeropnameid = GlobalModelGetId();

  List<ListOpname> _listAssetByRoom = [];
  List<ListOpname> get listAssetByRoom => _listAssetByRoom;
  set listAssetByRoom(List<ListOpname> _model) {
    listAssetByRoom.addAll(_model);
  }

  List<ListOpname> _listAssetOpnamed = [];
  List<ListOpname> get listAssetOpnamed => _listAssetOpnamed;
  set listAssetOpnamed(List<ListOpname> _model) {
    listAssetOpnamed.addAll(_model);
  }

  void addBarcodeList(String sBarcode) {
    if (sBarcode.toString().trim() != "") {
      barcodeList.add(sBarcode);
      notifyListeners();
    }
  }

  void clearBarcodeList() {
    barcodeList.clear();
  }

  void clearList() {
    listOpname.data = null;
    listAssetByRoom.clear();
    listAssetOpnamed.clear();
  }

  void clearHeaderopnameid() {
    headeropnameid.data = null;
  }

  Future getOpnameID(String _site, String _room, String _user) async {
    //---> Get Opname ID
    Map<String, dynamic> params2() =>
        {"siteid": _site, "roomid": _room, "userid": _user};
    dynamic _opnameid = await getHeaderOpnameID(params2());

    if (_opnameid == false)
      bGetOpnameID = false;
    else
      headeropnameid = _opnameid;

    notifyListeners(); //--> Get Header Opname ID
  }

  Future fetchDataListOpname() async {
    List<ListOpname> _tmplist = [];
    List<ListOpname> _tmplistOpnamed = [];

    clearList();

    //---> Get List Asset Opname By Room
    Map<String, dynamic> params() =>
        {"headeropnameid": headeropnameid.data[0].id.toString()};
    dynamic _apiresult = await getDataListOpname(params());

    if (_apiresult == false)
      bFetchData = false;
    else {
      listOpname = _apiresult;

      listOpname.data.forEach((x) {
        if (x.statusopname == "Asset List")
          _tmplist.add(x);
        else
          _tmplistOpnamed.add(x);
      });

      listAssetByRoom = _tmplist;
      listAssetOpnamed = _tmplistOpnamed;
    }

    notifyListeners();
  }

  Future<bool> closeOpnameRoom() async {
    bShowLoading = true;
    notifyListeners();

    Map<String, dynamic> params() =>
        {"headeropnameid": headeropnameid.data[0].id.toString()};
    dynamic _apiresult = await updateStatusRoom(params());

    if (_apiresult == false) {
      bClosedRoom = false;
      bShowLoading = false;
      notifyListeners();
    } else {
      closeRoomResponse = _apiresult;
      if (closeRoomResponse.data[0].pesan == "Failed") {
        bClosedRoom = false;
        bShowLoading = false;
        notifyListeners();
      } else
        bClosedRoom = true;
    }

    return bClosedRoom;
  }

  Future<bool> uploadBarcodeList(String sUserid) async {
    bShowLoading = true;
    notifyListeners();

    //--> Convert String List to String
    var barcodeString = StringBuffer();
    barcodeList.forEach((item) {
      barcodeString.write(item + ",");
    });

    String sBarcodeUpload = barcodeString
        .toString()
        .substring(0, barcodeString.toString().length - 1);

    Map<String, dynamic> params() => {
          "headeropnameid": headeropnameid.data[0].id.toString(),
          "barcodelist": sBarcodeUpload,
          "userid": sUserid,
        };

    dynamic _apiresult = await uploadBarcodeOpname(params());

    if (_apiresult == false) {
      bUploadBarcode = false;
    } else {
      uploadOpname = _apiresult;
      if (uploadOpname.data[0].pesan == "Failed")
        bUploadBarcode = false;
      else
        bUploadBarcode = true;
    }

    bShowLoading = false;
    notifyListeners();

    return bUploadBarcode;
  }
}
