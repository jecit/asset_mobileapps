import 'package:flutter/widgets.dart';
import 'package:jec_asset_mobile/data/services/APIProvider_AssetOpname.dart';
import 'package:jec_asset_mobile/models/OpnameLocationModel.dart';

class OpnameLocationNotifier extends ChangeNotifier {
  List<OpnameLocation_PT> _modelPT = [];
  List<OpnameLocation_PT> get modelPT => _modelPT;
  set modelPT(List<OpnameLocation_PT> _model) {
    modelSite.clear();
    modelPT.addAll(_model);
  }

  List<OpnameLocation_Site> _modelSite = [];
  List<OpnameLocation_Site> get modelSite => _modelSite;
  set modelSite(List<OpnameLocation_Site> _model) {
    modelSite.clear();
    modelSite.addAll(_model);
  }

  List<OpnameLocation_Floor> _modelFloor = [];
  List<OpnameLocation_Floor> get modelFloor => _modelFloor;
  set modelFloor(List<OpnameLocation_Floor> _model) {
    modelFloor.clear();
    modelFloor.addAll(_model);
  }

  List<OpnameLocation_Room> _modelRoom = [];
  List<OpnameLocation_Room> get modelRoom => _modelRoom;
  set modelRoom(List<OpnameLocation_Room> _model) {
    modelRoom.clear();
    modelRoom.addAll(_model);
  }

  OpnameLocation_PT ptSelected;
  OpnameLocation_Site siteSelected;
  OpnameLocation_Floor floorSelected;
  OpnameLocation_Room roomSelected;
  OpnameLocationModel modelLocation = new OpnameLocationModel();

  Future<List<OpnameLocation_PT>> getDataOpnameLocation(String sUserid) async {
    List<OpnameLocation_PT> _listptTemp = [];

    clearData(); //untuk handle kalo back (pop)

    //--> Get Data From Server
    modelLocation = await getDataLocationOpname();

    //---->> Load Data PT
    var listPT = List<OpnameLocation_PT>.from(modelLocation.data
        .map((location) => OpnameLocation_PT.fromOpnameLocation(location)));
    //distinct rows
    dynamic counter = 0;
    listPT.forEach((x) {
      if (x.ptid != counter) {
        _listptTemp.add(x);
        counter = x.ptid;
      }
    });

    modelPT = _listptTemp;
    notifyListeners();

    return modelPT;
  }

  void clearData() {
    modelPT.clear();
    modelSite.clear();
    modelFloor.clear();
    modelRoom.clear();

    ptSelected = null;
    siteSelected = null;
    floorSelected = null;
  }

  void selectedPT(OpnameLocation_PT valueSelected) {
    List<OpnameLocation_Site> _listtemp = [];
    //---->> Update Value Selected
    ptSelected = valueSelected;
    siteSelected = null;
    floorSelected = null;
    roomSelected = null;

    //---->> Load Data Site
    var listSite = List<OpnameLocation_Site>.from(modelLocation.data
        .where((pt) => (pt.ptid == valueSelected.ptid))
        .map((location) => OpnameLocation_Site.fromOpnameLocation(location)));

    //distinct rows
    var counter = '0';
    listSite.forEach((x) {
      if (x.siteId != counter) {
        _listtemp.add(x);
        counter = x.siteId.toString();
      }
    });

    modelSite = _listtemp;

    notifyListeners();
  }

  void selectedSite(OpnameLocation_Site valueSelected) {
    List<OpnameLocation_Floor> _listtemp = [];
    //---->> Update Value Selected
    siteSelected = valueSelected;
    floorSelected = null;
    roomSelected = null;

    //---->> Load Data Floor
    var listFloor = List<OpnameLocation_Floor>.from(modelLocation.data
        .where((site) => (site.siteId == valueSelected.siteId))
        .map((location) => OpnameLocation_Floor.fromOpnameLocation(location)));

    //distinct rows
    var counter = '0';
    listFloor.forEach((x) {
      if (x.floorLevel != counter) {
        _listtemp.add(x);
        counter = x.floorLevel;
      }
    });

    modelFloor = _listtemp;

    notifyListeners();
  }

  void selectedFloor(OpnameLocation_Floor valueSelected) {
    //---->> Update Value Selected
    floorSelected = valueSelected;
    roomSelected = null;

    //---->> Load Data Room
    modelRoom = List<OpnameLocation_Room>.from(modelLocation.data
        .where((floor) => (floor.siteId == valueSelected.floorSiteID &&
            floor.floorLevel == valueSelected.floorLevel))
        .map((location) => OpnameLocation_Room.fromOpnameLocation(location)));

    notifyListeners();
  }

  void selectedRoom(OpnameLocation_Room valueSelected) {
    //------> Update Value Selected
    roomSelected = valueSelected;
    notifyListeners();
  }
}
