import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jec_asset_mobile/data/services/APIProvider_AssetRegister.dart';
import 'package:jec_asset_mobile/helper/widgets/fileConvert.dart';
import 'package:jec_asset_mobile/models/AssetInsertModel.dart';
import 'package:jec_asset_mobile/models/AssetRegisterModel.dart';
import 'package:jec_asset_mobile/models/GetFileNameUpload.dart';
import 'package:path/path.dart';

class AssetRegisterNotifier extends ChangeNotifier {
  String sErrorMessage = '';
  String sBarcode = '';
  String sImagePath1 = '';
  String sImagePath2 = '';
  String sImagePath3 = '';
  List filesDelete = [];

  bool sHowLoading = false;
  // bool get sHowLoading => _sHowLoading;
  // set sHowLoading(bool state) {
  //   sHowLoading = state;
  //   notifyListeners();
  // }

  AssetRegister modelAsset = new AssetRegister();
  // AssetRegister get modelAsset => _modelAsset;
  // set modelAsset(AssetRegister _model) {
  //   modelAsset = _model;
  //   notifyListeners();
  // }

  AssetInsert modelInsert = new AssetInsert();
  // AssetInsert get modelInsert => _modelInsert;
  // set modelInsert(dynamic _model) {
  //   modelInsert = _model;
  //   // notifyListeners();
  // }

  Future scanningQR() async {
    try {
      String scanResult = await BarcodeScanner.scan();
      sBarcode = scanResult;
      modelAsset.barcode = scanResult;
      notifyListeners();
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        sErrorMessage = "Camera Permission was Denied";
      } else {
        sErrorMessage = "Unknown Error $ex";
      }

      notifyListeners();
    }
  }

  Future imagePicker(String flag) async {
    //Take Image From Camera
    File img = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 15);

    // //Compress Image and Create New File
    if (img != null) {
      if (flag == '1') {
        sImagePath1 = img.path;
      } else if (flag == '2') {
        sImagePath2 = img.path;
      } else if (flag == '3') {
        sImagePath3 = img.path;
      }
      modelAsset.imagepath = img.parent.path.toString();

      notifyListeners();
    }
  }

  void loadingShow(bool _state) {
    sHowLoading = _state;
    notifyListeners();
  }

  void clearAllData() {
    sErrorMessage = '';
    sBarcode = '';
    sImagePath1 = '';
    sImagePath2 = '';
    sImagePath3 = '';
    filesDelete = [];
    // modelAsset = null;
    // modelInsert = null;

    notifyListeners();
  }

  Future<bool> saveDataAssetLocally(String sAssetid) async {
    String sImg3 = "";

    try {
      File img1 = new File(sImagePath1);
      String sImg1 = basename(img1.path);

      File img2 = new File(sImagePath2);
      String sImg2 = basename(img2.path);

      if (sImagePath3 != '') {
        File img3 = new File(sImagePath3);
        sImg3 = basename(img3.path);
      }

      Map<String, dynamic> _params() =>
          modelAsset.toJsonImage(sImg1, sImg2, sImg3);
      Map<String, dynamic> addiitionalparams() =>
          {"assetid": sAssetid.toString(), "ispost": 0};

      Map finalParams = _params();
      finalParams.addAll(addiitionalparams());

      final sHasil = await postdataAssetDetail(finalParams);

      if (sHasil == false) {
        sErrorMessage = "Failed Connect to Server.";
        notifyListeners();
        return false;
      } else {
        modelInsert = sHasil;
        if (modelInsert.result == "success") {
          if (modelInsert.data[0].pesan == "Success")
            return true;
          else {
            sErrorMessage = "Oopss...!! Failed Post Data to Server.";
            notifyListeners();
            return false;
          }
        } else {
          sErrorMessage = "Oopss...!! Failed Save data Locally.";
          notifyListeners();
          return false;
        }
      }
    } catch (ex) {
      print(ex.toString());
      return false;
    }
  }

  Future<dynamic> postDataAsset(String sAssetid) async {
    String sImg1 = "", sImg2 = "", sImg3 = "";
    String img1Base64 = "", img2Base64 = "", img3Base64 = "";
    GetFileNameUpload fileNamesUploaded;

    try {
      File img1 = new File(sImagePath1);
      //--> tampung list image (local storage) yg akan dihapus.
      filesDelete.add(img1.path);
      //--> Convert to Base64
      img1Base64 = await compute(filetoBase64, img1);

      File img2 = new File(sImagePath2);
      //--> tampung list image (local storage) yg akan dihapus.
      filesDelete.add(img2.path);
      //--> Convert to Base64
      img2Base64 = await compute(filetoBase64, img2);

      if (sImagePath3 != '' &&
          sImagePath3.toString().substring(sImagePath3.toString().length - 1) !=
              "/") {
        File img3 = new File(sImagePath3);
        //--> tampung list image (local storage) yg akan dihapus.
        filesDelete.add(img3.path);
        //--> Convert to Base64
        img3Base64 = await compute(filetoBase64, img3);
      }

      List<Map<String, dynamic>> _images() => [
            {"data": img1Base64},
            {"data": img2Base64},
            if (sImagePath3 != '' &&
                sImagePath3
                        .toString()
                        .substring(sImagePath3.toString().length - 1) !=
                    "/")
              {"data": img3Base64}
          ];

      Map<String, dynamic> addiitionalparams() => {"Files": _images()};

      //---> Upload File dan get FileNamenya
      dynamic getFileName = await getFileNameUpload(addiitionalparams());

      if (getFileName == false) {
        sErrorMessage = "Failed upload image to server.";
        return false;
      } else {
        //---> Update File Name Yang baru
        fileNamesUploaded = getFileName;
        sImg1 = fileNamesUploaded.data.files[0].id.toString();
        sImg2 = fileNamesUploaded.data.files[1].id.toString();
        if (sImagePath3 != '')
          sImg3 = fileNamesUploaded.data.files[2].id.toString();

        modelAsset.imagepath = "https://service.jec.co.id/api/jec_api/images";

        Map<String, dynamic> _params() =>
            modelAsset.toJsonImage(sImg1, sImg2, sImg3);
        Map<String, dynamic> addiitionalparams2() =>
            {"assetid": sAssetid.toString(), "ispost": 1};

        Map finalParams = _params();
        finalParams.addAll(addiitionalparams2());

        //---> Update File Name dan Path File Ke Server DB
        final sHasil = await postdataAssetDetail(finalParams);

        if (sHasil == false) {
          sErrorMessage = "Failed Connect to Server.";
          notifyListeners();
          return false;
        } else {
          modelInsert = sHasil;
          if (modelInsert.result == "success") {
            if (modelInsert.data[0].pesan == "Success")
              return true;
            else {
              sErrorMessage = "Oopss...!! Failed Post Data to Server.";
              notifyListeners();
              return false;
            }
          } else {
            sErrorMessage = "Oopss...!! Failed Save data.";
            notifyListeners();
            return false;
          }
        }
      }
    } catch (ex) {
      sErrorMessage = "Oops..Error : ${ex.toString()}";
      notifyListeners();
      return false;
    }
  }
}
