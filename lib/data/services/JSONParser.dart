import 'dart:convert';
import 'package:http/http.dart' as http;

Future<List> apiRequestGet(String url) async {
  final response = await http
      .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

  if (response.statusCode == 200) {
    var jsonData = json.decode(response.body);
    return jsonData["data"]["d"];
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<Map<String, dynamic>> apiRequestPost(String url, Map params) async {
  final response = await http.post(Uri.encodeFull(url),
      headers: {"content-type": "application/json"}, body: json.encode(params));

  if (response.statusCode == 200) {
    var jsonData = json.decode(response.body);
    return jsonData;
  } else {
    return null;
  }
}
