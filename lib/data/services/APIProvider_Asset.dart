import 'package:jec_asset_mobile/data/services/JSONParser.dart';
import 'package:jec_asset_mobile/models/AssetNotifikasi.dart';

Future<dynamic> getListNotifikasi(Map params) async {
  try {
    AssetNotifikasi modelNotif;
    String url = 'https://service.jec.co.id/api/jec_api/asset/NotifikasiAsset';

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    if (params != '') {
      map.addAll(params);
    }

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    modelNotif = AssetNotifikasi.fromJson(parsed);

    return modelNotif;
  } catch (ex) {
    return false;
  }
}
