import 'package:jec_asset_mobile/data/services/JSONParser.dart';
import 'package:jec_asset_mobile/models/commonModels/GetIDmodel.dart';
import 'package:jec_asset_mobile/models/commonModels/ModifiedResponseModel.dart';
import 'package:jec_asset_mobile/models/OpnameLIstModel.dart';
import 'package:jec_asset_mobile/models/OpnameLocationModel.dart';

Future<dynamic> getDataLocationOpname() async {
  try {
    OpnameLocationModel modelInsert;
    String url =
        'https://service.jec.co.id/api/jec_api/asset/GetLocationOpname';

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    modelInsert = OpnameLocationModel.fromJson(parsed);

    return modelInsert;
  } catch (ex) {
    return false;
  }
}

Future<dynamic> getDataListOpname(Map params) async {
  try {
    OpnameListModel modelInsert;
    String url =
        'https://service.jec.co.id/api/jec_api/asset/GetListAssetOpname';

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    if (params != "") map.addAll(params);

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    modelInsert = OpnameListModel.fromJson(parsed);

    return modelInsert;
  } catch (ex) {
    return false;
  }
}

Future<dynamic> getPeriodOpnameID(Map params) async {
  try {
    GlobalModelGetId _model;
    String url =
        'https://service.jec.co.id/api/jec_api/asset/GetPeriodOpnameID';

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    if (params != "") map.addAll(params);

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    _model = GlobalModelGetId.fromJson(parsed);

    return _model;
  } catch (ex) {
    return false;
  }
}

Future<dynamic> getHeaderOpnameID(Map params) async {
  try {
    GlobalModelGetId _model;
    String url = 'https://service.jec.co.id/api/jec_api/asset/GetTransOpnameID';

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    if (params != "") map.addAll(params);

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    _model = GlobalModelGetId.fromJson(parsed);

    return _model;
  } catch (ex) {
    return false;
  }
}

Future<dynamic> updateStatusRoom(Map params) async {
  try {
    ModifiedResponseModel _modelResponse;
    String url = 'https://service.jec.co.id/api/jec_api/asset/CloseOpnameRoom';

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    if (params != "") map.addAll(params);

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    _modelResponse = ModifiedResponseModel.fromJson(parsed);

    return _modelResponse;
  } catch (ex) {
    return false;
  }
}

Future<dynamic> uploadBarcodeOpname(Map params) async {
  try {
    ModifiedResponseModel _modelResponse;
    String url =
        'https://service.jec.co.id/api/jec_api/asset/InsertOpnameDetail';

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    if (params != "") map.addAll(params);

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    _modelResponse = ModifiedResponseModel.fromJson(parsed);

    return _modelResponse;
  } catch (ex) {
    return false;
  }
}
