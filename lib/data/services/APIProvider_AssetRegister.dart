import 'dart:async';
import 'package:jec_asset_mobile/models/AssetCategoryModel.dart';
import 'package:jec_asset_mobile/models/AssetInsertModel.dart';
import 'package:jec_asset_mobile/models/AssetLocationModel.dart';
import 'package:jec_asset_mobile/models/AssetRegisterListModel.dart';
import 'package:jec_asset_mobile/models/AssetRegisterViewModel.dart';
import 'package:jec_asset_mobile/models/AssetSearchModel.dart';
import 'package:jec_asset_mobile/models/AssetTypeModel.dart';
import 'package:jec_asset_mobile/models/GetFileNameUpload.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'JSONParser.dart';

Future<AssetRole> fetchdataRole(String sUserid) async {
  AssetRole _assetrole;

  String url = 'https://service.jec.co.id/api/jec_api/asset/GetDataAccessRole';

  Map<String, dynamic> mandatoryParams() => {
        "token": "JcbBOOzZ",
        "ClinicName": "JEC@CORP",
        "UserID": sUserid.toString()
      };

  Map map = mandatoryParams();

  Map<String, dynamic> parsed = (await apiRequestPost(url, map));
  _assetrole = AssetRole.fromJson(parsed);

  return _assetrole;
}

Future<AssetLocationModel> fetchdataLocation(Map params) async {
  AssetLocationModel _assetLocation;

  String url =
      'https://service.jec.co.id/api/jec_api/asset/GetDataLocation_All';

  Map<String, dynamic> mandatoryParams() =>
      {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
  Map map = mandatoryParams();
  if (params != '') {
    map.addAll(params);
  }

  Map<String, dynamic> parsed = (await apiRequestPost(url, map));
  _assetLocation = AssetLocationModel.fromJson(parsed);

  return _assetLocation;
}

Future<AssetCategory> fetchdataCategory(Map params) async {
  AssetCategory _assetCategory;

  String url = 'https://service.jec.co.id/api/jec_api/asset/GetAssetCategory';

  Map<String, dynamic> mandatoryParams() =>
      {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
  Map map = mandatoryParams();

  if (params != '') {
    map.addAll(params);
  }

  Map<String, dynamic> parsed = (await apiRequestPost(url, map));
  _assetCategory = AssetCategory.fromJson(parsed);

  return _assetCategory;
}

Future<AssetType> fetchdataAssetType() async {
  AssetType _assetType;

  String url = 'https://service.jec.co.id/api/jec_api/asset/GetDataAssetType';

  Map<String, dynamic> mandatoryParams() =>
      {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
  Map map = mandatoryParams();

  Map<String, dynamic> parsed = (await apiRequestPost(url, map));
  _assetType = AssetType.fromJson(parsed);

  return _assetType;
}

Future<AssetSearch> fetchdataAssetSearch(Map params) async {
  AssetSearch _assetSearch;

  String url =
      'https://service.jec.co.id/api/jec_api/asset/GetDataAsset_Search';

  Map<String, dynamic> mandatoryParams() =>
      {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
  Map map = mandatoryParams();

  if (params != '') {
    map.addAll(params);
  }

  Map<String, dynamic> parsed = (await apiRequestPost(url, map));
  _assetSearch = AssetSearch.fromJson(parsed);

  return _assetSearch;
}

Future<AssetRegisterListModel> fetchdataAssetRegsiterList(Map params) async {
  AssetRegisterListModel _assetmodel;

  String url =
      'https://service.jec.co.id/api/jec_api/asset/GetDataAssetRegister';

  Map<String, dynamic> mandatoryParams() =>
      {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
  Map map = mandatoryParams();

  if (params != '') {
    map.addAll(params);
  }

  Map<String, dynamic> parsed = (await apiRequestPost(url, map));
  _assetmodel = AssetRegisterListModel.fromJson(parsed);

  return _assetmodel;
}

Future<AssetRegisterView> fetchDetailAssetRegsiter(Map params) async {
  AssetRegisterView _assetmodel;

  String url =
      'https://service.jec.co.id/api/jec_api/asset/GetDetailAssetRegister';

  Map<String, dynamic> mandatoryParams() =>
      {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
  Map map = mandatoryParams();

  if (params != '') {
    map.addAll(params);
  }

  Map<String, dynamic> parsed = (await apiRequestPost(url, map));
  _assetmodel = AssetRegisterView.fromJson(parsed);

  return _assetmodel;
}

Future<AssetInsert> postdataAsset(Map params) async {
  AssetInsert modelInsert;
  String url =
      'https://service.jec.co.id/api/jec_api/asset/InsertAssetRegister';

  Map<String, dynamic> mandatoryParams() =>
      {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
  Map map = mandatoryParams();

  if (params != '') {
    map.addAll(params);
  }

  Map<String, dynamic> parsed = (await apiRequestPost(url, map));
  modelInsert = AssetInsert.fromJson(parsed);

  return modelInsert;
}

Future<dynamic> getFileNameUpload(Map params) async {
  GetFileNameUpload fileUploaded;
  try {
    String url =
        "https://service.jec.co.id/api/jec_api/asset/GetFileNameUpload";

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    if (params != '') {
      map.addAll(params);
    }

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    fileUploaded = GetFileNameUpload.fromJson(parsed);
    return fileUploaded;
  } catch (ex) {
    return false;
  }
}

Future<dynamic> postdataAssetDetail(Map params) async {
  try {
    AssetInsert modelInsert;
    String url =
        'https://service.jec.co.id/api/jec_api/asset/InsertDetailAsset';

    Map<String, dynamic> mandatoryParams() =>
        {"token": "JcbBOOzZ", "ClinicName": "JEC@CORP"};
    Map map = mandatoryParams();

    if (params != '') {
      map.addAll(params);
    }

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    modelInsert = AssetInsert.fromJson(parsed);

    return modelInsert;
  } catch (ex) {
    return false;
  }
}
