import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'JSONParser.dart';

Future<UserAccount> fetchlogin(Map params) async {
  try {
    UserAccount _modelLogin;
    String url =
        'https://service.jec.co.id/api/jec_api/AlbedoAccess/AlbedoAccess';

    Map<String, dynamic> mandatoryParams() => {"token": "JcbBOOzZ"};
    Map map = mandatoryParams();

    if (params != '') {
      map.addAll(params);
    }

    Map<String, dynamic> parsed = (await apiRequestPost(url, map));
    _modelLogin = UserAccount.fromJson(parsed);

    return _modelLogin;
  } catch (err) {
    return null;
  }
}
