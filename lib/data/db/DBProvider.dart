import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "Asset.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE AssetLocation ("
          "ptid INTEGER,"
          "ptname TEXT,"
          "siteid TEXT,"
          "site TEXT,"
          "floorlevel TEXT,"
          "floordesc TEXT,"
          "roomid TEXT,"
          "roomname TEXT"
          ")");
    });
  }

  deleteAll_Location() async {
    final db = await database;
    db.rawDelete("Delete * from AssetLocation");
  }

  // insertData_Location(DataLocation location) async {
  //   final db = await database;
  //   var res = await db.insert("location", location.);
  //   return res;
  // }
}
