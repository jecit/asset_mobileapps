import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/base/base_routes.dart';
import 'package:jec_asset_mobile/notifiers/AssetRegisterNotifier.dart';
import 'package:jec_asset_mobile/notifiers/OpnameTransactionNotifier.dart';
import 'package:jec_asset_mobile/presenter/LoginPresenter.dart';
import 'package:jec_asset_mobile/notifiers/OpnameLocationNotifier.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginPresenter>.value(value: LoginPresenter()),
        ChangeNotifierProvider<OpnameLocationNotifier>.value(
            value: OpnameLocationNotifier()),
        ChangeNotifierProvider<AssetOpnameTransactionNotifier>.value(
            value: AssetOpnameTransactionNotifier()),
        ChangeNotifierProvider<AssetRegisterNotifier>.value(
            value: AssetRegisterNotifier()),
      ],
      child: MaterialApp(
        theme: ThemeData(
            primarySwatch: Colors.blue,
            textTheme: Theme.of(context).textTheme.apply(fontFamily: "breuer")),
        debugShowCheckedModeBanner:
            false, //--> Untuk menghilangkan pita merah pojok kanan atas (mode debug)
        title: 'JEC Asset Access',
        // home: new LoginPage(),
        initialRoute: '/',
        onGenerateRoute: Base_Routes.generateRoute,
      ),
    );
  }
}
