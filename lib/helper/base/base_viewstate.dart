import 'package:flutter/widgets.dart';
import 'package:jec_asset_mobile/helper/enums/viewstate.dart';

class Base_ViewState extends ChangeNotifier {
  ViewState _state = ViewState.Idle;

  ViewState get state => _state;

  void setState(ViewState viewState) {
    _state = viewState;
    notifyListeners();
  }
}
