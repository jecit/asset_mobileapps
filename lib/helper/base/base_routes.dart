import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/underconstruction_page.dart';
import 'package:jec_asset_mobile/presenter/AssetLocationPresenter.dart';
import 'package:jec_asset_mobile/presenter/AssetRegisterListPresenter.dart';
import 'package:jec_asset_mobile/presenter/HomePresenter.dart';
import 'package:jec_asset_mobile/view/AssetOpname/AssetOpnameAdd.dart';
import 'package:jec_asset_mobile/view/AssetOpname/AssetOpnameList.dart';
import 'package:jec_asset_mobile/view/AssetOpname/AssetOpnameLocation.dart';
import 'package:jec_asset_mobile/view/AssetOpname/AssetOpnameTransaction.dart';
import 'package:jec_asset_mobile/view/AssetRegister/AssetLocation.dart';
import 'package:jec_asset_mobile/view/AssetRegister/AssetRegisterList.dart';
import 'package:jec_asset_mobile/view/Home/home.dart';
import 'package:jec_asset_mobile/view/Login/Login.dart';

class Base_Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case '/home':
        return MaterialPageRoute(
            builder: (_) => Home(args, BasicHomePresenter()));
      case '/assetregister':
        return MaterialPageRoute(
            builder: (_) =>
                AssetRegisterList(args, BasicAssetRegisterListPresenter()));
      case '/assetregister/add':
        return MaterialPageRoute(
            builder: (_) => AssetLocation(args, BasicAssetLocationPresenter()));
      case '/assetopname':
        return MaterialPageRoute(
            builder: (_) => AssetOpnameLocation(
                  userAccount: args,
                ));
      case '/assetopname/list':
        return MaterialPageRoute(
            builder: (_) => AssetOpnametransaction(
                  sStatusRoom: args,
                ));
      case '/assetopname/add':
        return MaterialPageRoute(builder: (_) => AssetOpanemAdd());
      case '/assetopname/listasset':
        return MaterialPageRoute(builder: (_) => AssetOpnameListByRoom());
      case '/constructionpage':
        return MaterialPageRoute(builder: (_) => UnderConstructionPage());
    }
  }
}
