import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/presenter/HomePresenter.dart';
import 'package:jec_asset_mobile/view/Home/home.dart';

Widget CustomForm(BuildContext _context, Column _content) {
  return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0.0),
      child: Container(
        width: MediaQuery.of(_context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          // border: Border.all(color: Colors.grey, width: 1.0),
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 5.0, // has the effect of softening the shadow
              spreadRadius: 2.0, // has the effect of extending the shadow
              // offset: Offset(
              //   10.0, // horizontal, move right 10
              //   3.0, // vertical, move down 10
              // ),
            ),
          ],
        ),
        child: IntrinsicWidth(
          child: _content,
        ),
      ));
}

Widget CustomFormLogin(BuildContext _context, Column _content) {
  return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0.0),
      child: Container(
        width: MediaQuery.of(_context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          // border: Border.all(color: Colors.grey, width: 1.0),
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.blueGrey,
              blurRadius: 10.0, // has the effect of softening the shadow
              spreadRadius: 1.0, // has the effect of extending the shadow
              // offset: Offset(
              //   10.0, // horizontal, move right 10
              //   3.0, // vertical, move down 10
              // ),
            ),
          ],
        ),
        child: IntrinsicWidth(
          child: _content,
        ),
      ));
}

Widget CustomFormList(Column _content) {
  return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0.0),
      child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 5.0, // has the effect of softening the shadow
                spreadRadius: 2.0, // has the effect of extending the shadow
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CustomVerticalDivider(72.0, 1.0, Color(0xff19AFFF), 5.0),
              Expanded(
                child: _content,
              ),
            ],
          )));
}

Widget CustomAppBar(
    String _caption, BuildContext _context, UserAccount _userdata) {
  return AppBar(
    backgroundColor: Colors.blue,
    centerTitle: true,
    title: new Text(_caption, style: TextStyle(fontFamily: "breuer")),
    actions: <Widget>[
      InkWell(
        onTap: () {
          Navigator.of(_context).push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  new Home(_userdata, new BasicHomePresenter())));
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Icon(Icons.home),
        ),
      )
    ],
  );
}

Widget CustomAppBarNoHome(String _caption, BuildContext _context) {
  return AppBar(
    centerTitle: true,
    backgroundColor: Colors.blue,
    title: Padding(
      padding: EdgeInsets.only(left: 20.0),
      child: new Text(_caption, style: TextStyle(fontFamily: "breuer")),
    ),
  );
}

Widget CustomTextField(
    String _caption,
    TextEditingController _textcontroller,
    IconData _icon,
    IconData _iconTrailing,
    bool _enabled,
    Function _functiontrailing) {
  return ListTile(
    leading: Icon(
      _icon,
      color: Colors.blue,
    ),
    trailing: _iconTrailing != null
        ? _functiontrailing == null
            ? Icon(
                _iconTrailing,
                color: Colors.blueGrey,
              )
            : GestureDetector(
                onTap: _functiontrailing,
                child: Icon(
                  _iconTrailing,
                  color: Colors.blueGrey,
                ))
        : SizedBox(
            width: 0.0,
          ),
    title: TextField(
      controller: _textcontroller,
      enabled: _enabled,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(hintText: _caption),
      style: TextStyle(fontFamily: "breuer", fontSize: 12.0),
    ),
  );
}

Widget CustomPictureBox(double sizeWidth, double sizeHeight,
    Function _getpicture, String _pathImage) {
  var btnCamera = Container(
    alignment: FractionalOffset.center,
    width: 30.0,
    height: 30.0,
    decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.all(const Radius.circular(30.0))),
    child: Icon(
      Icons.add_a_photo,
      color: Colors.white,
      size: 15.0,
    ),
  );

  return Stack(
    children: <Widget>[
      Container(
          width: sizeWidth,
          height: sizeHeight,
          decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              image: new DecorationImage(
                  fit: BoxFit.fill, image: new AssetImage(_pathImage)))),
      Positioned(
          top: -3,
          right: -3,
          child: GestureDetector(
            child: btnCamera,
            onTap: _getpicture,
          ))
    ],
  );
}

class CustommTextView extends StatelessWidget {
  String label;
  String data;

  CustommTextView(this.label, this.data);

  @override
  Widget build(BuildContext context) {
    return new Padding(
        padding: EdgeInsets.only(left: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              label.toString(),
              style: TextStyle(
                  color: Colors.blue[300],
                  fontSize: 10.0,
                  fontFamily: "breuer"),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 1.0),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                data.toString(),
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 14.0,
                    fontFamily: "breuer"),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 1.0),
            ),
          ],
        ));
  }
}

class CustomDivider extends StatelessWidget {
  final double height;
  final Color color;

  CustomDivider(this.height, this.color);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Divider(
          height: this.height,
          color: this.color,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5.0),
        ),
      ],
    );
  }
}

class CustomVerticalDivider extends StatelessWidget {
  final double height;
  final double marginleft;
  final Color color;
  final double bordersize;

  CustomVerticalDivider(
      this.height, this.marginleft, this.color, this.bordersize);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: this.height, //
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 3.0),
          child: Container(
            height: this.height, //screenSize.height / 9,
            margin: EdgeInsetsDirectional.only(start: this.marginleft), //2.0),
            decoration: BoxDecoration(
              border: Border(
                  left: BorderSide(color: this.color, width: this.bordersize)),
            ),
          ),
        ),
      ),
    );
  }
}

class CustomListData extends StatelessWidget {
  final String label;
  final String value;
  final double fontsize;
  final Color fontcolor;

  CustomListData(this.label, this.value, this.fontsize, this.fontcolor);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          this.label,
          style: TextStyle(
              fontSize: this.fontsize,
              fontFamily: "breuer",
              color: this.fontcolor),
        ),
        Text(
          this.value,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              fontFamily: "breuer",
              fontSize: this.fontsize,
              color: this.fontcolor),
        ),
      ],
    );
  }
}
