import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class loadingScreen extends StatelessWidget {
  final String sText;
  final bool bShow;

  const loadingScreen({Key key, this.sText, this.bShow}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const spinkit = SpinKitFadingCircle(
      color: Colors.white,
      size: 50.0,
    );

    final screensize = MediaQuery.of(context).size;

    return bShow == false
        ? SizedBox(
            width: 0.0,
            height: 0.0,
          )
        : Container(
            color: Colors.black12,
            width: screensize.width,
            height: screensize.height,
            child: Center(
              child: Container(
                width: 300.0,
                height: 90.0,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 5.0, // has the effect of softening the shadow
                      spreadRadius:
                          2.0, // has the effect of extending the shadow
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      spinkit,
                      SizedBox(
                        width: 20.0,
                      ),
                      Text(
                        sText,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
