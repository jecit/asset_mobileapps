import 'dart:convert';
import 'dart:io';

String filetoBase64(File _file) {
  try {
    List<int> fileBytes = _file.readAsBytesSync();
    String fileBase64 = base64Encode(fileBytes);

    return fileBase64;
  } catch (ex) {
    return ex.toString();
  }
}
