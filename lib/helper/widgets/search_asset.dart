import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/models/AssetSearchModel.dart';

// Future<DataAssetSearch> showAssetSearch(BuildContext context, DataCategory assetgroup_select) async =>
//     await showSearch<DataAssetSearch>(
//       context: context,
//       delegate: AssetSearchDelegate(assetgroup_select),
//     );

class ShowAssetSearch extends SearchDelegate<DataAssetSearch> {
  final List<DataAssetSearch> assets;

  ShowAssetSearch(this.assets);

  // final List<DataAssetSearch> assets = new List();

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Text('Result');
    // final _presenter = BasicAssetLocationPresenter();
    // return null;
    // return FutureBuilder(
    //     future: _presenter.searchPlaces(_assetGroup_select, query),
    //     builder: (context, AsyncSnapshot<List<DataAssetSearch>> snapshot) {
    //       if (snapshot.hasData) {
    //         return Container(
    //           child: ListView(
    //             children: snapshot.data
    //                 .map(
    //                   (p) => AssetTile(
    //                           assets: p,
    //                           onTap: () => close(context, p),
    //                       ),
    //                 )
    //                 .toList(),
    //           ),
    //         );
    //       } else if (snapshot.hasError) {
    //         return Container(
    //           child: Center(
    //             child: Text(
    //               '${snapshot.error}',
    //               textAlign: TextAlign.center,
    //             ),
    //           ),
    //         );
    //       } else {
    //         return Container(
    //           child: Center(
    //             child: CircularProgressIndicator(),
    //           ),
    //         );
    //       }
    //     });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<DataAssetSearch> suggestionList = query.isEmpty
        ? []
        : assets
            .where((asset) =>
                asset.assetType.toLowerCase().contains(query.toLowerCase()))
            .toList();

    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: (context, index) => AssetTile(
        assetType: suggestionList[index].assetType.toString(),
        assetgroup: suggestionList[index].assetGroup.toString(),
        onTap: null,
      ),
    );
  }
}

class AssetTile extends StatelessWidget {
  AssetTile({this.assetType, this.assetgroup, this.onTap});
  final String assetType;
  final String assetgroup;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(assetType),
      subtitle: Text(assetgroup),
      leading: Icon(Icons.category),
      onTap: onTap,
    );
  }
}
