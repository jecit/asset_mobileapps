import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/data/services/APIProvider_AssetRegister.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:toast/toast.dart';

class menuBar extends StatelessWidget {
  final UserAccount _userAccount;

  menuBar(this._userAccount);

  Future<bool> valid_login(UserAccount _user) async {
    bool bValid;
    AssetRole _cekAkses;

    _cekAkses = await fetchdataRole(_user.data.d[0].userId.toString());

    _cekAkses.data.length == 0 ? bValid = false : bValid = true;

    return bValid;
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new ListView(
        children: <Widget>[
          new UserAccountsDrawerHeader(
              accountName: new Text(
                  this._userAccount.data.d[0].userName.toString(),
                  style: TextStyle(
                      fontFamily: "breuer",
                      color: Colors.white,
                      fontSize: 18.0)),
              accountEmail: new Text(
                  this._userAccount.data.d[0].email.toString(),
                  style: TextStyle(
                      fontFamily: "breuer",
                      color: Colors.white,
                      fontSize: 14.0)),
              currentAccountPicture: new CircleAvatar(
                backgroundImage: AssetImage("assets/img/Face.png"),
                backgroundColor: Colors.white54,
              ),
              decoration: new BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/img/bg3.jpg"), fit: BoxFit.fill),
              )),
          new ListTile(
            title: new Text(
              "Register New Asset",
              style: TextStyle(fontFamily: "breuer"),
            ),
            trailing: new Icon(Icons.note_add),
            onTap: () async {
              bool bValid = await valid_login(this._userAccount);

              if (bValid) {
                Navigator.of(context).pop();
                Navigator.of(context)
                    .pushNamed('/assetregister', arguments: this._userAccount);
              } else {
                Navigator.of(context).pop();
                Toast.show(
                    "You dont have permission for this menu. Please Contact your administrator...!!",
                    context,
                    duration: Toast.LENGTH_LONG,
                    gravity: Toast.CENTER);
              }
            },
          ),
          new ListTile(
            title: new Text(
              "Asset Opname",
              style: TextStyle(fontFamily: "breuer"),
            ),
            trailing: new Icon(Icons.assignment),
            onTap: () async {
              // Navigator.of(context).pop();
              // Navigator.of(context).pushNamed('/constructionpage');

//              bool bValid = await valid_login(this._userAccount);
              bool bValid = true;

              if (bValid) {
                Navigator.of(context).pop();
                Navigator.of(context)
                    .pushNamed('/assetopname', arguments: this._userAccount);
              } else {
                Navigator.of(context).pop();
                Toast.show(
                    "You dont have permission for this menu. Please Contact your administrator...!!",
                    context,
                    duration: Toast.LENGTH_LONG,
                    gravity: Toast.CENTER);
              }
            },
          ),
          new ListTile(
            title: new Text(
              "Logout",
              style: TextStyle(fontFamily: "breuer"),
            ),
            trailing: new Icon(Icons.lock),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed('/');
            },
          )
        ],
      ),
    );
  }
}
