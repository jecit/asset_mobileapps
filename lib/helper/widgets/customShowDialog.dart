import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

Widget customShowDialog(
    String sImagePath, String sTitle, String sPesan, dynamic okFunction) {
  return AssetGiffyDialog(
    image: Image.asset(
      sImagePath,
      fit: BoxFit.fill,
    ),
    title: Text(
      sTitle,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
    ),
    entryAnimation: EntryAnimation.TOP_RIGHT,
    description: Text(
      sPesan,
      textAlign: TextAlign.center,
      style: TextStyle(),
    ),
    onOkButtonPressed: () {
      okFunction();
      print('Pressed OK Button');
    },
  );
}
