import 'dart:io';
import 'dart:ui';
import 'package:image/image.dart';

File fileCompress(File _fileImage) {
  // Read a jpeg image from file.
  Image image = decodeImage(_fileImage.readAsBytesSync());

  // Resize the image to a 120x? thumbnail (maintaining the aspect ratio).
  Image thumbnail = copyResize(image, width: 300);

  //Get Path File
  String dirPath = _fileImage.parent.path.toString();
  String sFileName = DateTime.now().millisecondsSinceEpoch.toString() + '.png';

  // Save the thumbnail as a PNG.
  File newImage = new File(dirPath.toString() + '/' + sFileName)
    ..writeAsBytesSync(encodePng(thumbnail));

  // //Delete Original Image
  // _fileImage.delete();

  return newImage;
}
