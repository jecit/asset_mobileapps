import 'package:jec_asset_mobile/data/services/APIProvider_Login.dart';
import 'package:jec_asset_mobile/helper/base/base_viewstate.dart';
import 'package:jec_asset_mobile/helper/enums/viewstate.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';

class LoginPresenter extends Base_ViewState {
  UserAccount _model;
  UserAccount get model => _model;
  set model(UserAccount val) {
    _model = val;
    notifyListeners();
  }

  String sErrMessage = "";

  Future<bool> loginClick(String sUserID, String sPass) async {
    try {
      setState(ViewState.Busy);

      if (sUserID == "" || sPass == "") {
        sErrMessage = "User Name dan Password Tidak Boleh Kosong.";
        setState(ViewState.Idle);
        return false;
      }

      UserAccount datauser;
      Map<String, dynamic> params() =>
          {"UserName": sUserID.toString(), "Password": sPass.toString()};

      datauser = await fetchlogin(params());

      if (datauser.result == "success") {
        if (datauser.data.d[0].status == "1") {
          sErrMessage = "";
          this.model = datauser;
          setState(ViewState.Idle);
          return true;
        } else {
          sErrMessage = "Invalid UserName or Password.";
          setState(ViewState.Idle);
          return false;
        }
      } else {
        sErrMessage = "Failed Connect to server.";
        setState(ViewState.Idle);
        return false;
      }
    } catch (ex) {
      sErrMessage = "Failed Connect to server.";
      setState(ViewState.Idle);
      return false;
    }
  }
}
