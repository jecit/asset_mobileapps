import 'package:jec_asset_mobile/data/services/APIProvider_AssetRegister.dart';
import 'package:jec_asset_mobile/models/AssetRegisterListModel.dart';
import 'package:jec_asset_mobile/view/AssetRegister/IAssetRegisterList.dart';

class AssetRegisterListPresenter {
  set view(Interface_assetRegsterList _value) {}
  void loadListAsset(String _username) {}
}

class BasicAssetRegisterListPresenter implements AssetRegisterListPresenter {
  AssetRegisterListModel _model;
  Interface_assetRegsterList _interface;

  BasicAssetRegisterPresenter() {
    this._model = AssetRegisterListModel();
  }

  @override
  set view(Interface_assetRegsterList _value) {
    _interface = _value;
    this._interface.refreshPage(this._model);
  }

  @override
  void loadListAsset(String _username) async {
    Map<String, dynamic> params() => {"UserID": _username.toString()};
    _model = await fetchdataAssetRegsiterList(params());

    this._interface.refreshPage(this._model);
  }
}
