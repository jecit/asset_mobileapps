import 'package:jec_asset_mobile/data/services/APIProvider_AssetRegister.dart';
import 'package:jec_asset_mobile/models/AssetCategoryModel.dart';
import 'package:jec_asset_mobile/models/AssetInsertModel.dart';
import 'package:jec_asset_mobile/models/AssetLocationModel.dart';
import 'package:jec_asset_mobile/models/AssetRegisterModel.dart';
import 'package:jec_asset_mobile/models/AssetTypeModel.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/view/AssetRegister/IAssetLocation.dart';

class AssetLocationPresenter {
  set view(Interface_assetLocation _value) {}
  loadDataCombo(String sUserID) {}
  saveAssetRegister(
      UserAccount user,
      String sPTID,
      String sSiteID,
      String sRoomID,
      String sCatID,
      String sSubCatID,
      String sTypeID,
      String sAssetName,
      String sAssetCode) {}
  String sErrMsg;
}

class BasicAssetLocationPresenter implements AssetLocationPresenter {
  Interface_assetLocation _interface;
  AssetCategory _modelCategory1;
  AssetType _modelAssetType;
  AssetLocationModel _modelLocation;
  String sErr;
  AssetRegister _model;

  BasicAssetLocationPresenter() {
    this._modelCategory1 = AssetCategory();
    this._modelAssetType = AssetType();
    this._model = AssetRegister();
  }

  @override
  set view(Interface_assetLocation _value) {
    _interface = _value;
    _interface.refreshPage(
        this._modelLocation, this._modelCategory1, this._modelAssetType);
  }

  @override
  void loadDataCombo(String sUserID) async {
    //--> Fetch Data Asset Location
    Map<String, dynamic> paramslocation() => {"UserID": sUserID.toString()};
    _modelLocation = await fetchdataLocation(paramslocation());

    //--> Fetch Data Asset Category
    Map<String, dynamic> params1() => {"CategoryLevel": "1"};
    _modelCategory1 = await fetchdataCategory(params1());
    _modelAssetType = await fetchdataAssetType();

    _interface.refreshPage(
        this._modelLocation, this._modelCategory1, this._modelAssetType);
  }

  @override
  Future<dynamic> saveAssetRegister(
      UserAccount user,
      String sPTID,
      String sSiteID,
      String sRoomID,
      String sCatID,
      String sSubCatID,
      String sTypeID,
      String sAssetName,
      String sAssetCode) async {
    AssetInsert modelInsert;
    // bool bValidSave = false;

    if (sPTID == "" || sSiteID == "" || sRoomID == "" || sRoomID == "null") {
      sErr = "Oopss...!! Data Location Harus Lengkap.";
      // bValidSave = false;
      return false;
    } else if (sCatID == "" ||
        sTypeID == "" ||
        sCatID == "null" ||
        sTypeID == "null") {
      sErr = "Oopss...!! Data Category Harus Lengkap.";
      // bValidSave = false;
      return false;
    } else if (sAssetName == "" || sAssetName == "null") {
      sErr = "Oopss...!! Asset Name Harus di isi.";
      // bValidSave = false;
      return false;
    } else {
      //---> Post Data
      try {
        _model.assetpt = sPTID;
        _model.assetsite = sSiteID;
        _model.assetroom = sRoomID;
        _model.assetcat1 = sCatID;
        _model.assetcat2 = sSubCatID;
        _model.assetcat3 = sTypeID;
        _model.assetname = sAssetName;
        _model.assetcode = sAssetCode;

        Map<String, dynamic> _params() => _model.toJson2();
        Map<String, dynamic> addiitionalparams() =>
            {"userid": user.data.d[0].userId.toString()};

        Map finalParams = _params();
        finalParams.addAll(addiitionalparams());

        modelInsert = await postdataAsset(finalParams);

        // bValidSave = true;
        return modelInsert;
      } catch (ex) {
        sErr = ex.toString();
        return false;
      }
    }
  }

  @override
  set sErrMsg(String _sErrMsg) {
    sErrMsg = _sErrMsg;
  }

  String get sErrMsg => sErr;
}
