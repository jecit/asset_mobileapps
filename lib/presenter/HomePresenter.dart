import 'package:jec_asset_mobile/data/services/APIProvider_Asset.dart';
import 'package:jec_asset_mobile/models/AssetNotifikasi.dart';
import 'package:jec_asset_mobile/models/model.dart';
import 'package:jec_asset_mobile/view/Home/IHome.dart';
import 'package:http/http.dart' as http;

class HomePresenter {
  set view(Interface_home value) {}
  void scanningQR() {}
  void delete_file() {}
  void loadListNotifikasi(String _userid) {}
}

class BasicHomePresenter implements HomePresenter {
  QRData _model;
  AssetNotifikasi _modelNotif;
  Interface_home _interface;

  BasicHomePresenter() {
    this._model = QRData();
  }

  @override
  set view(Interface_home value) {
    _interface = value;
    this._interface.refreshPage(this._model, this._modelNotif);
  }

  @override
  void scanningQR() async {
    // try {
    //   String sQrResult = await BarcodeScanner.scan();
    //   _model.sIDBarcode = sQrResult;
    //   // _model.sIDBarcode = "001011992";
    //   _model.bValidScan = true;
    // } on PlatformException catch (ex) {
    //   if (ex.code == BarcodeScanner.CameraAccessDenied) {
    //     _model.bValidScan = false;
    //     _model.sIDBarcode = "Scan failed";
    //   }
    // }

    // this._interface.refreshPage(this._model);
  }

  @override
  void delete_file() async {
    var url = 'http://172.16.20.25:80/UploadHelper/deleteFile.php';
    var response =
        await http.post(url, body: {'filename': '1562725536225.png'});
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
  }

  @override
  void loadListNotifikasi(String _userid) async {
    Map<String, dynamic> params() => {"userid": _userid.toString()};
    _modelNotif = await getListNotifikasi(params());

    this._interface.refreshPage(this._model, this._modelNotif);
  }
}
