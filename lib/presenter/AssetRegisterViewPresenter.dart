import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:jec_asset_mobile/data/services/APIProvider_AssetRegister.dart';
import 'package:jec_asset_mobile/helper/enums/isvalid.dart';
import 'package:jec_asset_mobile/helper/widgets/fileConvert.dart';
import 'package:jec_asset_mobile/models/AssetInsertModel.dart';
import 'package:jec_asset_mobile/models/AssetRegisterViewModel.dart';
import 'package:jec_asset_mobile/models/GetFileNameUpload.dart';
import 'package:jec_asset_mobile/view/AssetRegister/IAssetRegisterViewer.dart';

class AssetRegisterViewPresenter {
  set view(Interface_assetRegisterViewer _value) {}
  void getDetailAssetRegister(String _id) {}
  scanningQR() {}
  void postDataAsset(String assetid, AssetRegisterView _modelAsset,
      Interface_assetRegisterViewer interface) {}
}

class BasicAssetRegisterViewPresenter implements AssetRegisterViewPresenter {
  AssetRegisterView _model;
  AssetInsert _modelInsert;
  Interface_assetRegisterViewer _interface;
  IsValid _isValid = IsValid.Valid;
  List _filesDelete = [];

  BasicAssetRegisterPresenter() {
    this._model = new AssetRegisterView();
    this._modelInsert = new AssetInsert();
    // this._interface = new Interface_assetRegisterViewer();
  }

  @override
  set view(Interface_assetRegisterViewer _value) {
    _interface = _value;
    this._interface.refreshPage(
        this._model, this._modelInsert, IsValid.Valid, _filesDelete);
  }

  @override
  void getDetailAssetRegister(String _id) async {
    Map<String, dynamic> params() => {"id": _id};
    _model = await fetchDetailAssetRegsiter(params());
    this._interface.refreshPage(
        this._model, this._modelInsert, IsValid.Valid, _filesDelete);
  }

  @override
  Future scanningQR() async {
    try {
      // String sQrResult = await FlutterBarcodeScanner.scanBarcode("#009922", "Abort", true, ScanMode.DEFAULT);
      String sQrResult = await BarcodeScanner.scan();
      if (sQrResult.toString() != "")
        _model.data[0].assetBarcode = sQrResult.toString();
      else
        _model.data[0].assetBarcode = '';
    } on FormatException {
      print("Cancel Scanning");
      _model.data[0].assetBarcode = '';
    }

    this._interface.refreshPage(
        this._model, this._modelInsert, IsValid.Valid, _filesDelete);
  }

  @override
  void postDataAsset(String sAssetid, AssetRegisterView _modelAsset,
      Interface_assetRegisterViewer interface) async {
    String sImg1 = "", sImg2 = "", sImg3 = "";
    String img1Base64 = "", img2Base64 = "", img3Base64 = "";
    GetFileNameUpload fileNamesUploaded;
    DataAssetView dataAsset;

    _model = _modelAsset;
    dataAsset = _modelAsset.data[0];
    _interface = interface;

    _isValid = IsValid.Valid;

    try {
      File img1 = new File(dataAsset.image1);
      //--> Compress Image
      // File imgCompress1 = await compute(fileCompress, img1);
      //--> Update Image Yang sudah di kompress
      _filesDelete.add(img1.path);
      // _filesDelete.add(imgCompress1.path);
      //--> Convert to Base64
      img1Base64 = await compute(filetoBase64, img1);

      File img2 = new File(dataAsset.image2);
      //--> Compress Image
      // File imgCompress2 = await compute(fileCompress, img2);
      //--> Update Image Yang sudah di kompress
      _filesDelete.add(img2.path);
      // _filesDelete.add(imgCompress2.path);
      //--> Convert to Base64
      img2Base64 = await compute(filetoBase64, img2);

      if (dataAsset.image3 != null &&
          dataAsset.image3
                  .toString()
                  .substring(dataAsset.image3.toString().length - 1) !=
              "/" &&
          FileSystemEntity.typeSync(dataAsset.image3) !=
              FileSystemEntityType.notFound) {
        File img3 = new File(dataAsset.image3);
        //--> Compress Image
        // File imgCompress3 = await compute(fileCompress, img3);
        //--> Update Image Yang sudah di kompress
        _filesDelete.add(img3.path);
        // _filesDelete.add(imgCompress3.path);
        //--> Convert to Base64
        img3Base64 = await compute(filetoBase64, img3);
      }

      List<Map<String, dynamic>> _images() => [
            {"data": img1Base64},
            {"data": img2Base64},
            if (dataAsset.image3 != null &&
                dataAsset.image3
                        .toString()
                        .substring(dataAsset.image3.toString().length - 1) !=
                    "/" &&
                FileSystemEntity.typeSync(dataAsset.image3) !=
                    FileSystemEntityType.notFound)
              {"data": img3Base64}
          ];

      Map<String, dynamic> additionalparams() => {"Files": _images()};

      //---> Upload File dan get FileNamenya
      dynamic getFileName = await getFileNameUpload(additionalparams());

      if (getFileName == false)
        _isValid = IsValid.Invalid;
      else {
        //---> Update File Name Yang baru
        fileNamesUploaded = getFileName;
        sImg1 = fileNamesUploaded.data.files[0].id.toString();
        sImg2 = fileNamesUploaded.data.files[1].id.toString();
        if (dataAsset.image3 != null &&
            dataAsset.image3
                    .toString()
                    .substring(dataAsset.image3.toString().length - 1) !=
                "/" &&
            FileSystemEntity.typeSync(dataAsset.image3) !=
                FileSystemEntityType.notFound)
          sImg3 = fileNamesUploaded.data.files[2].id.toString();

        dataAsset.imagepath = "https://service.jec.co.id/api/jec_api/images";

        Map<String, dynamic> _params() =>
            dataAsset.toJsonImage(sImg1, sImg2, sImg3);
        Map<String, dynamic> addiitionalparams2() =>
            {"assetid": sAssetid.toString(), "ispost": 1};

        Map finalParams = _params();
        finalParams.addAll(addiitionalparams2());

        //---> Update File Name dan Path File Ke Server DB
        this._modelInsert = await postdataAssetDetail(finalParams);
      }

      this
          ._interface
          .refreshPage(this._model, this._modelInsert, _isValid, _filesDelete);
    } catch (ex) {
      print(ex);
      this._interface.refreshPage(
          this._model, this._modelInsert, IsValid.Invalid, _filesDelete);
    }
  }
}
