import 'dart:io';
import 'package:async/async.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:http/http.dart' as http;
import 'package:jec_asset_mobile/data/services/APIProvider_AssetRegister.dart';
import 'package:jec_asset_mobile/helper/enums/isvalid.dart';
import 'package:jec_asset_mobile/helper/widgets/fileCompress.dart';
import 'package:jec_asset_mobile/helper/widgets/fileConvert.dart';
import 'package:jec_asset_mobile/models/AssetInsertModel.dart';
import 'package:jec_asset_mobile/models/AssetRegisterModel.dart';
import 'package:jec_asset_mobile/models/GetFileNameUpload.dart';
import 'package:jec_asset_mobile/view/AssetRegister/IRegisterAsset.dart';
import 'package:path/path.dart';

class AssetRegisterPresenter {
  set view(Interface_assetRegister _value) {}
  scanningQR() {}
  void saveDataAsset(AssetRegister dataAsset, String userID) {}
  saveDataAssetLocally(String assetid, AssetRegister dataAsset) {}
  postDataAsset(String assetid, AssetRegister dataAsset) {}
}

class BasicAssetRegisterPresenter implements AssetRegisterPresenter {
  AssetRegister _model;
  AssetInsert _modelInsert;
  Interface_assetRegister _interface = new Interface_assetRegister();
  IsValid _isValid = IsValid.Valid;
  List _filesDelete = [];

  BasicAssetRegisterPresenter() {
    this._model = AssetRegister();
    this._modelInsert = AssetInsert();
  }

  @override
  set view(Interface_assetRegister _value) {
    _interface = _value;
    this._interface.refreshPage(
        this._model, this._modelInsert, IsValid.Valid, _filesDelete);
  }

  @override
  Future scanningQR() async {
    try {
      // String sQrResult = await FlutterBarcodeScanner.scanBarcode("#009922", "Abort", true, ScanMode.DEFAULT);
      String sQrResult = await BarcodeScanner.scan();
      if (sQrResult.toString() != "")
        _model.barcode = sQrResult.toString();
      else
        _model.barcode = '';
    } on FormatException {
      print("Cancel Scanning");
      _model.barcode = '';
    }

    this._interface.refreshPage(
        this._model, this._modelInsert, IsValid.Valid, _filesDelete);
  }

  @override
  void saveDataAsset(AssetRegister dataAsset, String userID) async {
    bool postImage = await postImageAsset(dataAsset);

    if (postImage) {
      File img1 = new File(dataAsset.image1);
      String sImg1 = basename(img1.path);

      File img2 = new File(dataAsset.image2);
      String sImg2 = basename(img2.path);

      File img3 = new File(dataAsset.image3);
      String sImg3 = basename(img3.path);

      Map<String, dynamic> _params() => dataAsset.toJson(sImg1, sImg2, sImg3);
      Map<String, dynamic> addiitionalparams() => {"userid": userID.toString()};

      Map finalParams = _params();
      finalParams.addAll(addiitionalparams());

      this._modelInsert = await postdataAsset(finalParams);
    } else {
      this._modelInsert.result = "failed";
    }

    this._interface.refreshPage(
        this._model, this._modelInsert, IsValid.Valid, _filesDelete);
  }

  Future<bool> postImageAsset(AssetRegister dataAsset) async {
    File img1 = new File(dataAsset.image1);
    File img2 = new File(dataAsset.image2);
    File img3 = new File(dataAsset.image3);

    var uri = Uri.parse("http://172.16.20.25:80/UploadHelper/uploadAsset.php");

    var request = new http.MultipartRequest("POST", uri);

    var streamImg1 =
        new http.ByteStream(DelegatingStream.typed(img1.openRead()));
    final lengthImg1 = await img1.length();
    var multipartImg1 = new http.MultipartFile("image1", streamImg1, lengthImg1,
        filename: basename(img1.path));

    request.files.add(multipartImg1);

    var streamImg2 =
        new http.ByteStream(DelegatingStream.typed(img2.openRead()));
    final lengthImg2 = await img2.length();
    var multipartImg2 = new http.MultipartFile("image2", streamImg2, lengthImg2,
        filename: basename(img2.path));

    request.files.add(multipartImg2);

    var streamImg3 =
        new http.ByteStream(DelegatingStream.typed(img3.openRead()));
    final lengthImg3 = await img3.length();
    var multipartImg3 = new http.MultipartFile("image3", streamImg3, lengthImg3,
        filename: basename(img3.path));

    request.files.add(multipartImg3);

    var response = await request.send();

    if (response.statusCode == 200) {
      print("Upload Successfull");
      return true;
    } else {
      print("Upload Successfull");
      return false;
    }
  }

  @override
  Future<dynamic> saveDataAssetLocally(
      String sAssetid, AssetRegister dataAsset) async {
    String sImg3 = "";

    try {
      File img1 = new File(dataAsset.image1);
      String sImg1 = basename(img1.path);

      File img2 = new File(dataAsset.image2);
      String sImg2 = basename(img2.path);

      if (dataAsset.image3 != null) {
        File img3 = new File(dataAsset.image3);
        sImg3 = basename(img3.path);
      }

      Map<String, dynamic> _params() =>
          dataAsset.toJsonImage(sImg1, sImg2, sImg3);
      Map<String, dynamic> addiitionalparams() =>
          {"assetid": sAssetid.toString(), "ispost": 0};

      Map finalParams = _params();
      finalParams.addAll(addiitionalparams());

      dynamic sHasil = await postdataAssetDetail(finalParams);

      return sHasil;
    } catch (ex) {
      print(ex.toString());
      return false;
    }
  }

  @override
  void postDataAsset(String sAssetid, AssetRegister dataAsset) async {
    String sImg1 = "", sImg2 = "", sImg3 = "";
    String img1Base64 = "", img2Base64 = "", img3Base64 = "";
    GetFileNameUpload fileNamesUploaded;

    _isValid = IsValid.Valid;

    try {
      File img1 = new File(dataAsset.image1);
      //---> Compress File Sizenya
      File imgCompress1 = await compute(fileCompress, img1);
      //--> Update Image Yang sudah di kompress
      _filesDelete.add(img1.path);
      // _filesDelete.add(imgCompress1.path);
      //--> Convert to Base64
      img1Base64 = await compute(filetoBase64, img1);

      File img2 = new File(dataAsset.image2);
      //---> Compress File Sizenya
      // File imgCompress2 = await compute(fileCompress, img2);
      //--> Update Image Yang sudah di kompress
      _filesDelete.add(img2.path);
      // _filesDelete.add(imgCompress2.path);
      //--> Convert to Base64
      img2Base64 = await compute(filetoBase64, img2);

      if (dataAsset.image3 != null &&
          dataAsset.image3
                  .toString()
                  .substring(dataAsset.image3.toString().length - 1) !=
              "/") {
        File img3 = new File(dataAsset.image3);
        //---> Compress File Sizenya
        // File imgCompress3 = await compute(fileCompress, img3);
        //--> Update Image Yang sudah di kompress
        _filesDelete.add(img1.path);
        _filesDelete.add(imgCompress1.path);
        //--> Convert to Base64
        img3Base64 = await compute(filetoBase64, img3);
      }

      List<Map<String, dynamic>> _images() => [
            {"data": img1Base64},
            {"data": img2Base64},
            if (dataAsset.image3 != null &&
                dataAsset.image3
                        .toString()
                        .substring(dataAsset.image3.toString().length - 1) !=
                    "/")
              {"data": img3Base64}
          ];

      Map<String, dynamic> addiitionalparams() => {"Files": _images()};

      //---> Upload File dan get FileNamenya
      dynamic getFileName = await getFileNameUpload(addiitionalparams());

      if (getFileName == false)
        _isValid = IsValid.Invalid;
      else {
        //---> Update File Name Yang baru
        fileNamesUploaded = getFileName;
        sImg1 = fileNamesUploaded.data.files[0].id.toString();
        sImg2 = fileNamesUploaded.data.files[1].id.toString();
        if (dataAsset.image3 != null)
          sImg3 = fileNamesUploaded.data.files[2].id.toString();

        dataAsset.imagepath = "https://service.jec.co.id/api/jec_api/images";

        Map<String, dynamic> _params() =>
            dataAsset.toJsonImage(sImg1, sImg2, sImg3);
        Map<String, dynamic> addiitionalparams2() =>
            {"assetid": sAssetid.toString(), "ispost": 1};

        Map finalParams = _params();
        finalParams.addAll(addiitionalparams2());

        //---> Update File Name dan Path File Ke Server DB
        this._modelInsert = await postdataAssetDetail(finalParams);

        this._interface.refreshPage(
            this._model, this._modelInsert, _isValid, _filesDelete);
      }
    } catch (ex) {
      this._interface.refreshPage(
          this._model, this._modelInsert, IsValid.Invalid, _filesDelete);
    }
  }
}
