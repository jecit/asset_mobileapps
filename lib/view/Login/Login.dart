import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/enums/viewstate.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/presenter/LoginPresenter.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController ctl_UserID = new TextEditingController(text: "");
  final TextEditingController ctl_UserPass =
      new TextEditingController(text: "");

  @override
  void dispose() {
    ctl_UserID.dispose();
    ctl_UserPass.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screensize = MediaQuery.of(context).size;

    var backgroundLogin = Padding(
      padding: EdgeInsets.fromLTRB(5.0, 60.0, 5.0, 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Image.asset("assets/img/GedungJEC_Transparan.png"),
        ],
      ),
    );

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              //Background Image
              backgroundLogin,
              //Gradient
              Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                    Color.fromRGBO(62, 137, 231, 0.5),
                    Color.fromRGBO(251, 251, 251, 0.3)
                  ],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter))),
              Padding(padding: const EdgeInsets.all(15.0)),
              //Form login

              ListView(
                children: <Widget>[
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: screensize.width * 0.7,
                          width: 0.0,
                        ),
                        inputLogin(),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Text(
                              "Please use your ALBEDO Account for login.",
                              style: TextStyle(
                                  color: Colors.blueGrey, fontFamily: "breuer"),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget inputLogin() {
    final screensize = MediaQuery.of(context).size;
    final presenter = Provider.of<LoginPresenter>(context);

    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
      child: CustomFormLogin(
        context,
        Column(
          children: <Widget>[
            Padding(padding: const EdgeInsets.only(top: 30.0)),
            ListTile(
              leading: Icon(
                Icons.person,
                color: Colors.blue,
              ),
              title: TextField(
                controller: ctl_UserID,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(hintText: "Username"),
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontFamily: "breuer",
                    fontSize: 16.0),
              ),
            ),
            // Padding(padding: const EdgeInsets.all(10.0)),
            ListTile(
              leading: Icon(
                Icons.lock_open,
                color: Colors.blue,
              ),
              title: TextField(
                obscureText: true,
                controller: ctl_UserPass,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(hintText: "Password"),
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontFamily: "breuer",
                    fontSize: 16.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: (presenter.sErrMessage != "")
                  ? Text(presenter.sErrMessage,
                      style: TextStyle(
                          fontFamily: "breuer",
                          fontSize: 15.0,
                          color: Colors.redAccent))
                  : SizedBox(
                      height: 0.0,
                      width: 0.0,
                    ),
            ),
            (presenter.state == ViewState.Busy)
                ? const CircularProgressIndicator()
                : FlatButton(
                    padding: EdgeInsets.fromLTRB(
                        screensize.width / 5, 15.0, screensize.width / 5, 15.0),
                    color: Colors.blue,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                    child: Text(
                      "Login",
                      style: TextStyle(
                          fontFamily: "breuer",
                          fontSize: 23.0,
                          color: Colors.white),
                    ),
                    onPressed: () async {
                      var validLogin = await presenter.loginClick(
                          ctl_UserID.text, ctl_UserPass.text);
                      if (validLogin) {
                        Navigator.of(context)
                            .pushNamed('/home', arguments: presenter.model);
                      }
                    },
                  ),
            Padding(padding: const EdgeInsets.only(bottom: 8.0)),
          ],
        ),
      ),
    );
  }
}
