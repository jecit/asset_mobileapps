import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/models/AssetCategoryModel.dart';
import 'package:jec_asset_mobile/models/AssetInsertModel.dart';
import 'package:jec_asset_mobile/models/AssetLocationModel.dart';
import 'package:jec_asset_mobile/models/AssetTypeModel.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/presenter/AssetLocationPresenter.dart';
import 'package:jec_asset_mobile/presenter/AssetRegisterPresenter.dart';
import 'package:jec_asset_mobile/view/AssetRegister/IAssetLocation.dart';
import 'package:jec_asset_mobile/view/AssetRegister/RegisterAsset.dart';
import 'package:jec_asset_mobile/view/AssetRegister/AssetRegisterPost.dart';
import 'package:toast/toast.dart';

class AssetLocation extends StatefulWidget {
  final UserAccount _userAccount;
  final AssetLocationPresenter _presenter;

  AssetLocation(this._userAccount, this._presenter);

  @override
  _AssetLocationState createState() => _AssetLocationState();
}

class _AssetLocationState extends State<AssetLocation>
    implements Interface_assetLocation {
  AssetLocationModel _modelLocation;
  List<DataLocationPT> listAssetPT = new List<DataLocationPT>();
  List<DataLocationSite> listAssetSite = new List<DataLocationSite>();
  List<DataLocationFloor> listAssetFloor = new List<DataLocationFloor>();
  List<DataLocationRoom> listAssetRoom = new List<DataLocationRoom>();
  AssetCategory _modelCategory1;
  AssetType _modelAssetType;
  List<DataAssetType> listAssetType;
  String sAssetName;
  String sAssetCode;
  AssetInsert _modelInsert;
  AutoCompleteTextField searchAssetType;
  GlobalKey<AutoCompleteTextFieldState<DataAssetType>> key = new GlobalKey();
  TextEditingController txtAssetGroup = new TextEditingController(text: "");
  TextEditingController txtAssetSubGroup = new TextEditingController(text: "");
  TextEditingController txtAssetType = new TextEditingController(text: "");

  @override
  void initState() {
    super.initState();
    this.widget._presenter.view = this;
    this
        .widget
        ._presenter
        .loadDataCombo(this.widget._userAccount.data.d[0].userId.toString());
  }

  @override
  void dispose() {
    txtAssetGroup.dispose();
    txtAssetSubGroup.dispose();
    txtAssetType.dispose();
    // searchAssetType.textField.controller.dispose();
    super.dispose();
  }

  @override
  void refreshPage(AssetLocationModel modelLocation,
      AssetCategory modelCategory1, AssetType modelAssetType) {
    setState(() {
      this._modelLocation = modelLocation;
      this._modelCategory1 = modelCategory1;
      this._modelAssetType = modelAssetType;

      if (this._modelAssetType != null && this._modelLocation != null) {
        if (listAssetType == null) listAssetType = this._modelAssetType.data;
        if (listAssetPT == null || listAssetPT.length == 0) {
          //---> Load List PT
          var listPT = List<DataLocationPT>.from(this
              ._modelLocation
              .data
              .map((datapt) => DataLocationPT.fromLocation(datapt)));

          var olddataPT = 0;
          listPT.forEach((x) {
            if (x.ptid != olddataPT) {
              listAssetPT.add(x);
              olddataPT = x.ptid;
            }
          });
        }
        if (listAssetSite == null || listAssetSite.length == 0) {
          //---> Load List Site
          var listSite = List<DataLocationSite>.from(this
              ._modelLocation
              .data
              .map((datapt) => DataLocationSite.fromLocation(datapt)));

          var olddata = '0';
          listSite.forEach((x) {
            if (x.siteId != olddata) {
              listAssetSite.add(x);
              olddata = x.siteId;
            }
          });
        }
        if (listAssetFloor == null || listAssetFloor.length == 0) {
          //---> Load List Floor
          var listFloor = List<DataLocationFloor>.from(this
              ._modelLocation
              .data
              .map((datapt) => DataLocationFloor.fromLocation(datapt)));

          var olddata = '0';
          listFloor.forEach((x) {
            if (x.floorLevel != olddata) {
              listAssetFloor.add(x);
              olddata = x.floorLevel;
            }
          });
        }
        if (listAssetRoom == null || listAssetRoom.length == 0) {
          //---> Load List Room
          listAssetRoom = List<DataLocationRoom>.from(this
              ._modelLocation
              .data
              .map((datapt) => DataLocationRoom.fromLocation(datapt)));
        }
      }
    });
  }

  Widget rowAsset(DataAssetType asset) {
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0)),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              Icons.category,
              color: Colors.blue,
            ),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
            ),
            Text(
              asset.assetType,
              style: TextStyle(
                  fontFamily: "breuer", fontSize: 14.0, color: Colors.black87),
            ),
          ],
        ),
        CustomDivider(2.0, Colors.grey),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var btnNext = Padding(
      padding: const EdgeInsets.all(8.0),
      child: RaisedButton(
        onPressed: () async {
          if (this._modelInsert != null) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => new RegisterAsset(
                    this.widget._userAccount,
                    this._modelInsert.data[0].assetid.toString(),
                    BasicAssetRegisterPresenter())));
          } else {
            // await Future.delayed(Duration(seconds: 2));
            final saveValid = await this.widget._presenter.saveAssetRegister(
                this.widget._userAccount,
                _modelLocation.assetPTSelected?.ptid.toString() ?? "",
                _modelLocation.assetSiteSelected?.siteId.toString() ?? "",
                _modelLocation.assetRoomSelected?.siteRoomId.toString() ?? "",
                // _modelCategory1.categorySelected?.categoryId.toString() ?? "",
                _modelAssetType.assetTypeSelected?.assetGroupId.toString() ??
                    "",
                _modelAssetType.assetTypeSelected?.assetSubGroupId.toString() ??
                    "",
                _modelAssetType.assetTypeSelected?.assetTypeId.toString() ?? "",
                this.sAssetName.toString(),
                this.sAssetCode.toString());

            if (saveValid == false) {
              Toast.show(this.widget._presenter.sErrMsg.toString(), context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            } else {
              this._modelInsert = saveValid;

              // Navigator.of(context).push(MaterialPageRoute(
              //     builder: (BuildContext context) => new RegisterAsset(
              //         this.widget._userAccount,
              //         this._modelInsert.data[0].assetid.toString(),
              //         BasicAssetRegisterPresenter())));

              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => new RegisterAssetPost(
                      this.widget._userAccount,
                      this._modelInsert.data[0].assetid.toString())));
            }
          }
        },
        color: Colors.blue,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.add,
              color: Colors.white,
            ),
            Text(
              "Add New Asset",
              style: TextStyle(fontFamily: "breuer", color: Colors.white),
            ),
          ],
        ),
      ),
    );

    return Scaffold(
        appBar: CustomAppBarNoHome("Asset Register", context),
        body: _modelLocation == null ||
                listAssetPT == null ||
                listAssetSite == null ||
                listAssetRoom == null ||
                _modelCategory1 == null ||
                listAssetType == null
            ? Center(child: CircularProgressIndicator())
            : ListView(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                children: <Widget>[
                  // Asset Location
                  CustomForm(
                      context,
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                            child: Text(
                              "Asset Location",
                              style: TextStyle(
                                  fontFamily: "breuer",
                                  color: Colors.grey,
                                  fontSize: 15.0),
                            ),
                          ),
                          CustomDivider(2.0, Colors.grey),
                          ListTile(
                            leading: Icon(
                              Icons.domain,
                              color: Colors.blue,
                            ),
                            title: DropdownButton<DataLocationPT>(
                              value: _modelLocation.assetPTSelected,
                              items: listAssetPT
                                  .map((pt) => DropdownMenuItem<DataLocationPT>(
                                        child: Text(
                                          pt.ptName,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: "breuer"),
                                        ),
                                        value: pt,
                                      ))
                                  .toList(),
                              onChanged: (DataLocationPT value) {
                                setState(() {
                                  _modelLocation.assetSiteSelected = null;
                                  _modelLocation.assetFloorSelected = null;
                                  _modelLocation.assetRoomSelected = null;
                                  _modelLocation.assetPTSelected = value;

                                  var listSite = List<DataLocationSite>.from(
                                      this
                                          ._modelLocation
                                          .data
                                          .where((pt) => pt.ptid == value.ptid)
                                          .map((datapt) =>
                                              DataLocationSite.fromLocation(
                                                  datapt)));

                                  var olddata = '0';
                                  listAssetSite.clear();
                                  listSite.forEach((x) {
                                    if (x.siteId != olddata) {
                                      listAssetSite.add(x);
                                      olddata = x.siteId;
                                    }
                                  });
                                });
                              },
                              isExpanded: true,
                              hint: Text(
                                'Select Company',
                                style: TextStyle(
                                    fontSize: 14, fontFamily: "breuer"),
                              ),
                            ),
                          ),
                          ListTile(
                            leading: Icon(
                              Icons.domain,
                              color: Colors.blue,
                            ),
                            title: DropdownButton<DataLocationSite>(
                              value: _modelLocation.assetSiteSelected,
                              items: listAssetSite
                                  .map((site) =>
                                      DropdownMenuItem<DataLocationSite>(
                                        child: Text(
                                          site.site,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: "breuer"),
                                        ),
                                        value: site,
                                      ))
                                  .toList(),
                              onChanged: (DataLocationSite value) {
                                setState(() {
                                  _modelLocation.assetFloorSelected = null;
                                  _modelLocation.assetRoomSelected = null;
                                  _modelLocation.assetSiteSelected = value;

                                  var listFloor = List<DataLocationFloor>.from(
                                      this
                                          ._modelLocation
                                          .data
                                          .where((site) =>
                                              site.siteId == value.siteId)
                                          .map((datasite) =>
                                              DataLocationFloor.fromLocation(
                                                  datasite)));

                                  var olddata = '0';
                                  listAssetFloor.clear();
                                  listFloor.forEach((x) {
                                    if (x.floorLevel != olddata) {
                                      listAssetFloor.add(x);
                                      olddata = x.floorLevel;
                                    }
                                  });
                                });
                              },
                              isExpanded: true,
                              hint: Text(
                                'Select Site',
                                style: TextStyle(
                                    fontSize: 14, fontFamily: "breuer"),
                              ),
                            ),
                          ),
                          ListTile(
                            leading: Icon(
                              Icons.location_on,
                              color: Colors.blue,
                            ),
                            title: DropdownButton<DataLocationFloor>(
                              value: _modelLocation.assetFloorSelected,
                              items: listAssetFloor
                                  .map((floor) =>
                                      DropdownMenuItem<DataLocationFloor>(
                                        child: Text(
                                          floor.floorDesc,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: "breuer"),
                                        ),
                                        value: floor,
                                      ))
                                  .toList(),
                              onChanged: (DataLocationFloor value) {
                                setState(() {
                                  _modelLocation.assetRoomSelected = null;
                                  _modelLocation.assetFloorSelected = value;

                                  listAssetRoom = List<DataLocationRoom>.from(
                                      this
                                          ._modelLocation
                                          .data
                                          .where((floor) =>
                                              floor.siteId == value.siteId &&
                                              floor.floorLevel ==
                                                  value.floorLevel)
                                          .map((datafloor) =>
                                              DataLocationRoom.fromLocation(
                                                  datafloor)));
                                });
                              },
                              isExpanded: true,
                              hint: Text('Select Floor',
                                  style: TextStyle(
                                      fontSize: 14, fontFamily: "breuer")),
                            ),
                          ),
                          ListTile(
                            leading: Icon(
                              Icons.location_on,
                              color: Colors.blue,
                            ),
                            title: DropdownButton<DataLocationRoom>(
                              value: _modelLocation.assetRoomSelected,
                              items: listAssetRoom
                                  .map((room) =>
                                      DropdownMenuItem<DataLocationRoom>(
                                        child: Text(
                                          room.siteRoomName,
                                          style: TextStyle(
                                              fontSize: 12.5,
                                              fontFamily: "breuer"),
                                        ),
                                        value: room,
                                      ))
                                  .toList(),
                              onChanged: (DataLocationRoom value) {
                                setState(() {
                                  _modelLocation.assetRoomSelected = value;
                                });
                              },
                              isExpanded: true,
                              hint: Text('Select Room',
                                  style: TextStyle(
                                      fontSize: 14, fontFamily: "breuer")),
                            ),
                          ),
                        ],
                      )),

                  //Asset Category
                  CustomForm(
                      context,
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                            child: Text(
                              "Asset Category",
                              style: TextStyle(
                                  fontFamily: "breuer",
                                  color: Colors.grey,
                                  fontSize: 15.0),
                            ),
                          ),
                          // CustomDivider(2.0, Colors.grey),
                          // ListTile(
                          //   leading: Icon(
                          //     Icons.search,
                          //     color: Colors.blue,
                          //   ),
                          //   title: searchAssetType =
                          //       AutoCompleteTextField<DataAssetType>(
                          //     suggestionsAmount: 10,
                          //     key: key,
                          //     clearOnSubmit: false,
                          //     suggestions: listAssetType,
                          //     style: TextStyle(
                          //         color: Colors.black,
                          //         fontFamily: "breuer",
                          //         fontSize: 14.0),
                          //     decoration: InputDecoration(
                          //       hintText: "Search Asset Category",
                          //       hintStyle: TextStyle(
                          //           color: Colors.grey,
                          //           fontFamily: "breuer",
                          //           fontSize: 14.0),
                          //     ),
                          //     itemFilter: (asset, query) {
                          //       return asset.assetType
                          //           .toLowerCase()
                          //           .startsWith(query.toLowerCase());
                          //     },
                          //     itemSorter: (a, b) {
                          //       return a.assetType.compareTo(b.assetType);
                          //     },
                          //     itemSubmitted: (asset) {
                          //       setState(() {
                          //         searchAssetType.textField.controller.text = "";
                          //         _modelAssetType.assetTypeSelected = asset;
                          //         txtAssetGroup.text = asset.assetGroup;
                          //         txtAssetSubGroup.text = asset.assetSubGroup;
                          //         txtAssetType.text = asset.assetType;
                          //       });
                          //     },
                          //     itemBuilder: (context, asset) {
                          //       // ui for the autocompelete row
                          //       return rowAsset(asset);
                          //     },
                          //   ),
                          // ),

                          ListTile(
                            leading: Icon(
                              Icons.category,
                              color: Colors.blue,
                            ),
                            title: DropdownButton<DataCategory>(
                              value: _modelCategory1.categorySelected,
                              items: _modelCategory1.data
                                  .map((category1) =>
                                      DropdownMenuItem<DataCategory>(
                                        child: Text(
                                          category1.categoryName,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: "breuer"),
                                        ),
                                        value: category1,
                                      ))
                                  .toList(),
                              onChanged: (DataCategory value) {
                                setState(() {
                                  _modelAssetType.assetTypeSelected = null;
                                  listAssetType = this
                                      ._modelAssetType
                                      .data
                                      .where((assettype) =>
                                          assettype.assetGroupId ==
                                          value.categoryId)
                                      .toList();
                                  _modelCategory1.categorySelected = value;

                                  txtAssetGroup.text = "";
                                  txtAssetSubGroup.text = "";
                                  txtAssetType.text = "";
                                });
                              },
                              isExpanded: true,
                              hint: Text(
                                'Select Asset Group',
                                style: TextStyle(
                                    fontSize: 14, fontFamily: "breuer"),
                              ),
                            ),
                          ),

                          ListTile(
                            leading: Icon(
                              Icons.category,
                              color: Colors.blue,
                            ),
                            title: DropdownButton<DataAssetType>(
                              value: _modelAssetType.assetTypeSelected,
                              items: listAssetType
                                  .map((assettype) =>
                                      DropdownMenuItem<DataAssetType>(
                                        child: Text(
                                          assettype.assetType,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: "breuer"),
                                        ),
                                        value: assettype,
                                      ))
                                  .toList(),
                              onChanged: (DataAssetType value) {
                                setState(() {
                                  _modelAssetType.assetTypeSelected = value;
                                  txtAssetGroup.text = value.assetGroup;
                                  txtAssetSubGroup.text = value.assetSubGroup;
                                  txtAssetType.text = value.assetType;
                                });
                              },
                              isExpanded: true,
                              hint: Text(
                                'Select Asset Type',
                                style: TextStyle(
                                    fontSize: 14, fontFamily: "breuer"),
                              ),
                            ),
                          ),

                          CustomDivider(2.0, Colors.grey),
                          CustommTextView("Asset Type : ", txtAssetType.text),
                          CustomDivider(2.0, Colors.grey),
                          CustommTextView(
                              "Asset Sub Group : ", txtAssetSubGroup.text),
                          CustomDivider(2.0, Colors.grey),
                          CustommTextView("Asset Group : ", txtAssetGroup.text),
                          CustomDivider(2.0, Colors.grey),
                        ],
                      )),

                  // Asset Profile
                  CustomForm(
                      context,
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                            child: Text(
                              "Asset Profile",
                              style: TextStyle(
                                  fontFamily: "breuer",
                                  color: Colors.grey,
                                  fontSize: 15.0),
                            ),
                          ),
                          CustomDivider(2.0, Colors.grey),
                          ListTile(
                            leading: Icon(
                              Icons.create,
                              color: Colors.blue,
                            ),
                            title: TextField(
                              onChanged: (text) => this.sAssetName =
                                  text.toString().toUpperCase(),
                              keyboardType: TextInputType.text,
                              decoration:
                                  InputDecoration(hintText: "Asset Name"),
                              style: TextStyle(
                                  fontFamily: "breuer", fontSize: 12.0),
                            ),
                          ),
                          ListTile(
                            leading: Icon(
                              Icons.confirmation_number,
                              color: Colors.blue,
                            ),
                            title: TextField(
                              onChanged: (text) => this.sAssetCode = text,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  hintText: "Asset Code (optional)"),
                              style: TextStyle(
                                  fontFamily: "breuer", fontSize: 12.0),
                            ),
                          ),
                        ],
                      )),

                  // Button
                  CustomForm(
                      context,
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          btnNext,
                        ],
                      )),
                ],
              ));
  }
}
