import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/models/AssetRegisterListModel.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/presenter/AssetRegisterListPresenter.dart';
import 'package:jec_asset_mobile/presenter/HomePresenter.dart';
import 'package:jec_asset_mobile/view/AssetRegister/AssetRegisterList/ApprovedList.dart';
import 'package:jec_asset_mobile/view/AssetRegister/AssetRegisterList/LocalList.dart';
import 'package:jec_asset_mobile/view/AssetRegister/AssetRegisterList/ProcessList.dart';
import 'package:jec_asset_mobile/view/AssetRegister/AssetRegisterList/RejectList.dart';
import 'package:jec_asset_mobile/view/AssetRegister/IAssetRegisterList.dart';
import 'package:jec_asset_mobile/view/Home/home.dart';

class AssetRegisterList extends StatefulWidget {
  final UserAccount _userAccount;
  final AssetRegisterListPresenter _presenter;

  AssetRegisterList(this._userAccount, this._presenter);

  @override
  _AssetRegisterListState createState() => _AssetRegisterListState();
}

class _AssetRegisterListState extends State<AssetRegisterList>
    implements Interface_assetRegsterList {
  int _tabIndex = 0;
  AssetRegisterListModel model;
  List<DataAssetRegisterTemp> listmodel = new List<DataAssetRegisterTemp>();

  List<DataAssetRegisterTemp> listLocal = new List<DataAssetRegisterTemp>();
  List<DataAssetRegisterTemp> listProcess = new List<DataAssetRegisterTemp>();
  List<DataAssetRegisterTemp> listApproved = new List<DataAssetRegisterTemp>();
  List<DataAssetRegisterTemp> listReject = new List<DataAssetRegisterTemp>();

  @override
  void initState() {
    super.initState();

    this.widget._presenter.view = this;
    this
        .widget
        ._presenter
        .loadListAsset(this.widget._userAccount.data.d[0].userId.toString());
  }

  @override
  void refreshPage(AssetRegisterListModel _model) {
    setState(() {
      this.model = _model;

      if (this.model != null && this.model.data.length > 0) {
        listmodel = List<DataAssetRegisterTemp>.from(this.model.data.map(
            (dataasset) => DataAssetRegisterTemp.fromAssetRegister(dataasset)));

        //--> Break Data List
        listLocal = List<DataAssetRegisterTemp>.from(this
            .model
            .data
            .where((asset) =>
                asset.status.toString().toLowerCase() == "local storage")
            .map((dataasset) =>
                DataAssetRegisterTemp.fromAssetRegister(dataasset)));

        listProcess = List<DataAssetRegisterTemp>.from(this
            .model
            .data
            .where(
                (asset) => asset.status.toString().toLowerCase() == "submitted")
            .map((dataasset) =>
                DataAssetRegisterTemp.fromAssetRegister(dataasset)));

        listApproved = List<DataAssetRegisterTemp>.from(this
            .model
            .data
            .where(
                (asset) => asset.status.toString().toLowerCase() == "approved")
            .map((dataasset) =>
                DataAssetRegisterTemp.fromAssetRegister(dataasset)));

        listReject = List<DataAssetRegisterTemp>.from(this
            .model
            .data
            .where((asset) => asset.status.toString().toLowerCase() == "reject")
            .map((dataasset) =>
                DataAssetRegisterTemp.fromAssetRegister(dataasset)));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget pageList(int index) {
      switch (index) {
        case 0:
          return LocalList(
            listData: listLocal,
            userAccount: this.widget._userAccount,
          );
        case 1:
          return ProcessList(
              listData: listProcess, userAccount: this.widget._userAccount);
        case 2:
          return ApprovedList(
              listData: listApproved, userAccount: this.widget._userAccount);
        case 3:
          return RejectList(
              listData: listReject, userAccount: this.widget._userAccount);

          break;
        default:
          ProcessList(
              listData: listProcess, userAccount: this.widget._userAccount);
      }
    }

    return WillPopScope(
      onWillPop: () async => Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) =>
              new Home(this.widget._userAccount, new BasicHomePresenter()))),
      child: Scaffold(
          appBar: CustomAppBar(
              "Asset Registration List", context, this.widget._userAccount),
          body: model == null || listmodel == ""
              ? Center(child: CircularProgressIndicator())
              : pageList(_tabIndex),
          bottomNavigationBar: CurvedNavigationBar(
            index: _tabIndex,
            backgroundColor: Color(0xff0088D1),
            color: Color(0xff005685),
            buttonBackgroundColor: Color(0xffD17600),
            items: <Widget>[
              Icon(Icons.sd_storage, color: Colors.white, size: 30.0),
              Icon(Icons.cached, color: Colors.white, size: 30.0),
              Icon(Icons.check_box, color: Colors.white, size: 30.0),
              Icon(Icons.block, color: Colors.white, size: 30.0),
            ],
            onTap: (position) {
              setState(() {
                _tabIndex = position;
              });
            },
          ),
          floatingActionButton: FloatingActionButton.extended(
            icon: Icon(
              Icons.add_circle_outline,
              color: Colors.white,
              size: 25.0,
            ),
            label: Text(
              "Register Asset",
              style: TextStyle(fontFamily: "breuer", fontSize: 12.0),
            ),
            onPressed: () {
              Navigator.of(context).pushNamed('/assetregister/add',
                  arguments: this.widget._userAccount);
            },
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat),
    );
  }
}
