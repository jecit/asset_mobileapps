import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jec_asset_mobile/helper/enums/isvalid.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/models/AssetInsertModel.dart';
import 'package:jec_asset_mobile/models/AssetRegisterViewModel.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/presenter/AssetRegisterViewPresenter.dart';
import 'package:jec_asset_mobile/view/AssetRegister/IAssetRegisterViewer.dart';
import 'package:toast/toast.dart';

class AssetRegisterViewer extends StatefulWidget {
  final UserAccount _userAccount;
  final AssetRegisterViewPresenter _presenter;
  final String _idAssetRegister;

  AssetRegisterViewer(
      this._userAccount, this._presenter, this._idAssetRegister);

  @override
  _AssetRegisterViewerState createState() => _AssetRegisterViewerState();
}

class _AssetRegisterViewerState extends State<AssetRegisterViewer>
    implements Interface_assetRegisterViewer {
  AssetRegisterView model;
  String status = "";
  double heightLoading, widthLoading;
  String sErrMsg = "";
  TextEditingController ctlBarcode = TextEditingController(text: '');
  AssetInsert modelInsert;

  @override
  void refreshPage(AssetRegisterView _model, AssetInsert _modelInsert,
      IsValid _isValid, List _filesDelete) {
    setState(() {
      //Untuk Close Loading Panel
      if (heightLoading > 0) {
        heightLoading = 0;
        widthLoading = 0;
      }

      this.model = _model;
      this.modelInsert = _modelInsert;

      if (model != null) {
        status = model.data[0].status.toString();

        //-->> Untuk Load hasil Scan
        if (model.data[0].assetBarcode != null)
          ctlBarcode.text = model.data[0].assetBarcode;
      }
    });

    //--> Untuk Proses Upload Data Asset
    if (_isValid == IsValid.Invalid) {
      Toast.show(
          "Oopss...!! Failed Upload data, Failed Connect To Server.", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    } else {
      if (this.modelInsert != null) {
        if (this.modelInsert.result == "success") {
          if (this.modelInsert.data[0].pesan == "Success") {
            Navigator.of(context).pushNamed('/assetregister',
                arguments: this.widget._userAccount);

            //--> Kalo berhasil Upload, Delete Files yang ada di Local Storage.
            deleteJunkFiles(_filesDelete);
          } else
            Toast.show("Oopss...!! Failed Post Data to Server.", context,
                duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        } else
          Toast.show("Oopss...!! Failed Save data to Server.", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    heightLoading = 0.0;
    widthLoading = 0.0;

    this.widget._presenter.view = this;
    this.widget._presenter.getDetailAssetRegister(this.widget._idAssetRegister);
    print(this.widget._idAssetRegister);
  }

  bool validasiSave(AssetRegisterView _model) {
    if (_model.data[0].assetBarcode == "" ||
        _model.data[0].assetBarcode == null) {
      sErrMsg = "Oopss...!! Barcode Tidak Boleh Kosong.";
      return false;
    } else if (_model.data[0].image1 == "" ||
        _model.data[0].image1 == null ||
        _model.data[0].image1.substring(_model.data[0].image1.length - 1) ==
            "/" ||
        FileSystemEntity.typeSync(_model.data[0].image1) ==
            FileSystemEntityType.notFound ||
        _model.data[0].image2 == "" ||
        _model.data[0].image2 == null ||
        _model.data[0].image2.substring(_model.data[0].image2.length - 1) ==
            "/" ||
        FileSystemEntity.typeSync(_model.data[0].image2) ==
            FileSystemEntityType.notFound) {
      sErrMsg =
          "Oopss...!! Foto Unit Asset dan Foto Label Barcode Tidak Boleh Kosong.";
      return false;
    } else
      return true;
  }

  Widget loadingScreen(double _height, double _width) {
    return Container(
      child: Center(
          child: Container(
        decoration: BoxDecoration(
          color: Colors.grey,
        ),
        height: _height / 9,
        width: _width / 1.5,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            Padding(
              padding: const EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
              child: Text(
                "Loading.....   ",
                style: TextStyle(
                    fontFamily: "breuer",
                    fontSize: 20.0,
                    color: Colors.white54),
              ),
            ),
          ],
        ),
      )),
      height: _height,
      width: _width,
      color: Colors.black54,
    );
  }

  Future imagePicker(String flag) async {
    //Take Image From Camera
    File img = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 15);

    if (img != null) {
      //Load Image
      setState(() {
        if (flag == '1')
          model.data[0].image1 = img.path;
        else if (flag == '2')
          model.data[0].image2 = img.path;
        else if (flag == '3') model.data[0].image3 = img.path;

        model.data[0].imagepath = img.parent.path.toString();
      });
    }
  }

  Future deleteJunkFiles(List _filesDelete) async {
    //---> Delete Files yang sudah diUpload
    if (_filesDelete.length > 0) {
      //--> Delay dulu, supaya rebuild ui nya selesai.
      await Future.delayed(Duration(seconds: 1));

      //--> Delete Files
      int _index = 0;
      while (_filesDelete.length > _index) {
        if (FileSystemEntity.typeSync(_filesDelete[_index].toString()) !=
            FileSystemEntityType.notFound) {
          File junkfile = new File(_filesDelete[_index].toString());
          junkfile.delete();
        }
        _index++;
      }
    }
  }

  Widget frameImage(double _height, double _width, String _path, String _footer,
      String sImageStorage) {
    return Column(
      children: <Widget>[
        Container(
            width: _width,
            height: _height,
            decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: sImageStorage == "Local Storage"
                        ? ExactAssetImage(FileSystemEntity.typeSync(_path) ==
                                FileSystemEntityType.notFound
                            ? 'assets/img/noimage.jpg'
                            : _path)
                        : (_path.substring(_path.length - 1)) == "/"
                            ? ExactAssetImage('assets/img/noimage.jpg')
                            : NetworkImage(_path)))),
        Padding(
          padding: const EdgeInsets.all(2.0),
          child: Text(
            _footer,
            style: TextStyle(
                fontFamily: "breuer", fontSize: 12.0, color: Colors.grey),
          ),
        )
      ],
    );
  }

  var btnCamera = Container(
    alignment: FractionalOffset.center,
    width: 40.0,
    height: 40.0,
    decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.all(const Radius.circular(30.0))),
    child: Icon(
      Icons.add_a_photo,
      color: Colors.white,
      size: 20.0,
    ),
  );

  Uint8List loadImage(String imagePath) {
    File file = File(imagePath);
    Uint8List bytes = file.readAsBytesSync();
    return bytes;
  }

  Widget fotoPicker(double _height, double _width, String _path, String _footer,
      String _index) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
                width: _width,
                height: _height,
                decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: (_path == null ||
                                    _path == "" ||
                                    _path == "/" ||
                                    _path.substring(_path.length - 1) == "/" ||
                                    FileSystemEntity.typeSync(_path) ==
                                        FileSystemEntityType.notFound
                                ? ExactAssetImage('assets/img/noimage.jpg')
                                : new MemoryImage(loadImage(
                                    _path)) //new FileImage(File(_path))

                            )))),
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: Text(
                _footer,
                style: TextStyle(
                    fontFamily: "breuer", fontSize: 12.0, color: Colors.grey),
              ),
            )
          ],
        ),
        Positioned(
          top: -3,
          right: -3,
          child: GestureDetector(
              onTap: () {
                // setState(() {
                //   heightLoading = screensize.height;
                //   widthLoading = screensize.width;
                // });
                imagePicker(_index);
                // this.widget._presenter.imagePicker(_index);
              },
              child: btnCamera),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final screensize = MediaQuery.of(context).size;
    // this.widget._presenter.view = this;

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context)
            .pushNamed('/assetregister', arguments: this.widget._userAccount);
      },
      child: SafeArea(
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.blue,
              title: Padding(
                padding: EdgeInsets.only(left: 40.0),
                child: new Text("Asset Register Viewer",
                    style: TextStyle(fontFamily: "breuer")),
              ),
              leading: new IconButton(
                icon: Icon(Icons.list),
                onPressed: () {
                  Navigator.of(context).pushNamed('/assetregister',
                      arguments: this.widget._userAccount);
                },
              ),
            ),
            body: this.model == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Stack(
                    children: <Widget>[
                      ListView(
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        children: <Widget>[
                          // Asset Status
                          CustomForm(
                              context,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        20.0, 8.0, 0.0, 5.0),
                                    child: Text(
                                      "Register Status",
                                      style: TextStyle(
                                          fontFamily: "breuer",
                                          color: Colors.black87,
                                          fontSize: 15.0),
                                    ),
                                  ),
                                  CustomDivider(2.0, Colors.grey),
                                  Center(
                                    child: Text(
                                      model.data[0].status,
                                      style: TextStyle(
                                          fontFamily: "breuer",
                                          fontSize: 20.0,
                                          color: model.data[0].status ==
                                                  "Approved"
                                              ? Colors.green
                                              : model.data[0].status == "Reject"
                                                  ? Colors.red
                                                  : model.data[0].status ==
                                                          "Local Storage"
                                                      ? Colors.teal
                                                      : Colors.amber),
                                    ),
                                  ),
                                  CustomDivider(2.0, Colors.grey),
                                  model.data[0].status == "Reject"
                                      ? CustommTextView("Alasan Reject : ",
                                          model.data[0].alasanReject)
                                      : SizedBox(
                                          height: 0,
                                          width: 0,
                                        )
                                ],
                              )),
                          // Asset Profile
                          CustomForm(
                              context,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        20.0, 8.0, 0.0, 5.0),
                                    child: Text(
                                      "Asset Profile",
                                      style: TextStyle(
                                          fontFamily: "breuer",
                                          color: Colors.black87,
                                          fontSize: 15.0),
                                    ),
                                  ),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView(
                                      "Asset Name : ", model.data[0].assetName),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView(
                                      "Asset Code : ", model.data[0].assetCode),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView("Asset Barcode : ",
                                      model.data[0].assetBarcode),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView("Asset Group : ",
                                      model.data[0].assetGroup),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView("Asset Sub Group : ",
                                      model.data[0].assetSubGroup),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView(
                                      "Asset Type : ", model.data[0].assetType),
                                  CustomDivider(2.0, Colors.grey),
                                ],
                              )),
                          // Asset Location
                          CustomForm(
                              context,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        20.0, 8.0, 0.0, 5.0),
                                    child: Text(
                                      "Asset Location",
                                      style: TextStyle(
                                          fontFamily: "breuer",
                                          color: Colors.black87,
                                          fontSize: 15.0),
                                    ),
                                  ),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView(
                                      "Asset Company : ", model.data[0].ptName),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView(
                                      "Asset Site : ", model.data[0].siteName),
                                  CustomDivider(2.0, Colors.grey),
                                  CustommTextView(
                                      "Asset Room : ", model.data[0].roomName),
                                  CustomDivider(2.0, Colors.grey),
                                ],
                              )),
                          //Content Form Barcode Scanning
                          (status == "Local Storage")
                              ? CustomForm(
                                  context,
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(
                                            20.0, 8.0, 0.0, 5.0),
                                        child: Text(
                                          "Asset Barcode",
                                          style: TextStyle(
                                              fontFamily: "breuer",
                                              color: Colors.black,
                                              fontSize: 15.0),
                                        ),
                                      ),
                                      CustomDivider(2.0, Colors.grey),
                                      CustomTextField(
                                          "Barcode",
                                          ctlBarcode,
                                          Icons.view_column,
                                          Icons.camera_alt,
                                          false,
                                          this.widget._presenter.scanningQR),
                                    ],
                                  ))
                              : SizedBox(
                                  width: 0.0,
                                  height: 0.0,
                                ),

                          // Asset Image
                          CustomForm(
                              context,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        20.0, 8.0, 0.0, 5.0),
                                    child: Text(
                                      "Asset Image",
                                      style: TextStyle(
                                          fontFamily: "breuer",
                                          color: Colors.black87,
                                          fontSize: 15.0),
                                    ),
                                  ),
                                  CustomDivider(2.0, Colors.grey),
                                  Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: (status ==
                                                    "Local Storage")
                                                ? <Widget>[
                                                    fotoPicker(
                                                        screensize.width / 3,
                                                        screensize.width / 3,
                                                        model.data[0].image1,
                                                        "Foto Unit Asset",
                                                        "1"),
                                                    fotoPicker(
                                                        screensize.width / 3,
                                                        screensize.width / 3,
                                                        model.data[0].image2,
                                                        "Foto Label Barcode",
                                                        "2")
                                                  ]
                                                : <Widget>[
                                                    frameImage(
                                                        screensize.width / 3,
                                                        screensize.width / 3,
                                                        model.data[0].image1,
                                                        "Foto Unit Asset",
                                                        model.data[0].status
                                                            .toString()),
                                                    frameImage(
                                                        screensize.width / 3,
                                                        screensize.width / 3,
                                                        model.data[0].image2,
                                                        "Foto Label Barcode",
                                                        model.data[0].status
                                                            .toString()),
                                                  ]),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 10.0, 0.0, 1.0),
                                        ),
                                        Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: (status ==
                                                    "Local Storage")
                                                ? <Widget>[
                                                    fotoPicker(
                                                        screensize.width / 3,
                                                        screensize.width / 3,
                                                        model.data[0].image3,
                                                        "Foto Nomor SN",
                                                        "3")
                                                  ]
                                                : <Widget>[
                                                    frameImage(
                                                        screensize.width / 3,
                                                        screensize.width / 3,
                                                        model.data[0].image3,
                                                        "Foto Nomor SN",
                                                        model.data[0].status
                                                            .toString()),
                                                  ])
                                      ],
                                    ),
                                  )
                                ],
                              )),
                        ],
                      ),
                      loadingScreen(heightLoading, widthLoading)
                    ],
                  ),
            floatingActionButton: (status == "Local Storage")
                ? FloatingActionButton.extended(
                    icon: Icon(
                      Icons.cloud_upload,
                      color: Colors.white,
                    ),
                    label: Text(
                      "Upload",
                      style: TextStyle(fontFamily: "breuer", fontSize: 12.0),
                    ),
                    onPressed: () {
                      setState(() {
                        heightLoading = screensize.height;
                        widthLoading = screensize.width;
                      });

                      if (validasiSave(model)) {
                        this.widget._presenter.postDataAsset(
                            this.widget._idAssetRegister, this.model, this);
                      } else {
                        setState(() {
                          heightLoading = 0.0;
                          widthLoading = 0.0;
                        });

                        Toast.show(sErrMsg, context,
                            duration: Toast.LENGTH_SHORT,
                            gravity: Toast.BOTTOM);
                      }
                    },
                  )
                : SizedBox(
                    height: 0.0,
                    width: 0.0,
                  )),
      ),
    );
  }
}
