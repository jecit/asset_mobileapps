import 'dart:io';
import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/notifiers/AssetRegisterNotifier.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class RegisterAssetPost extends StatelessWidget {
  final UserAccount _userAccount;
  final String _assetID;

  RegisterAssetPost(this._userAccount, this._assetID, {Key key})
      : super(key: key);

  String sErrMsg = '';

  var btnCamera = Container(
    alignment: FractionalOffset.center,
    width: 40.0,
    height: 40.0,
    decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.all(const Radius.circular(30.0))),
    child: Icon(
      Icons.add_a_photo,
      color: Colors.white,
      size: 20.0,
    ),
  );

  Widget fotoPicker(String _path, String _footer, double _sizeframe) {
    return Column(
      children: <Widget>[
        Container(
            width: _sizeframe,
            height: _sizeframe,
            decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: _path == "null" || _path == ""
                        ? ExactAssetImage('assets/img/noimage.jpg')
                        : new FileImage(File(_path))))),
        Padding(
          padding: const EdgeInsets.all(2.0),
          child: Text(
            _footer,
            style: TextStyle(
                fontFamily: "breuer", fontSize: 12.0, color: Colors.grey),
          ),
        )
      ],
    );
  }

  Widget loadingScreen(double _height, double _width) {
    return Container(
      child: Center(
          child: Container(
        decoration: BoxDecoration(
          color: Colors.grey,
        ),
        height: _height / 9,
        width: _width / 1.5,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            Padding(
              padding: const EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
              child: Text(
                "Loading.....   ",
                style: TextStyle(
                    fontFamily: "breuer",
                    fontSize: 20.0,
                    color: Colors.white54),
              ),
            ),
          ],
        ),
      )),
      height: _height,
      width: _width,
      color: Colors.black54,
    );
  }

  bool validasiSave(String sBarcode, String sImg1, String sImg2) {
    if (sBarcode == "" || sBarcode == null) {
      sErrMsg = "Oopss...!! Barcode Tidak Boleh Kosong.";
      return false;
    } else if (sImg1 == "" || sImg1 == null || sImg2 == "" || sImg2 == null) {
      sErrMsg =
          "Oopss...!! Foto Unit Asset dan Foto Label Barcode Tidak Boleh Kosong.";
      return false;
    } else
      return true;
  }

  Future deleteJunkFiles(List _filesDelete) async {
    //---> Delete Files yang sudah diUpload
    if (_filesDelete.length > 0) {
      //--> Delay dulu, supaya rebuild ui nya selesai.
      await Future.delayed(Duration(seconds: 1));

      //--> Delete Files
      int _index = 0;
      while (_filesDelete.length > _index) {
        if (FileSystemEntity.typeSync(_filesDelete[_index].toString()) !=
            FileSystemEntityType.notFound) {
          File junkfile = new File(_filesDelete[_index].toString());
          junkfile.delete();
        }
        _index++;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final screensize = MediaQuery.of(context).size;
    final assetRegisNotifier =
        Provider.of<AssetRegisterNotifier>(context, listen: false);

    print("render page");
    return Scaffold(
      appBar: CustomAppBarNoHome("Asset Register", context),
      body: Stack(children: <Widget>[
        ListView(physics: ScrollPhysics(), shrinkWrap: true, children: <Widget>[
          //======================================================================================================//
          CustomForm(
              //Content Form Barcode Scanning
              context,
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                    child: Text(
                      "Asset Barcode",
                      style: TextStyle(
                          fontFamily: "breuer",
                          color: Colors.grey,
                          fontSize: 15.0),
                    ),
                  ),
                  CustomDivider(2.0, Colors.grey),
                  ListTile(
                      leading: Icon(
                        Icons.view_column,
                        color: Colors.blue,
                      ),
                      trailing: GestureDetector(
                          onTap: () async => assetRegisNotifier.scanningQR(),
                          child: Icon(
                            Icons.camera_alt,
                            color: Colors.blueGrey,
                          )),
                      title: Selector<AssetRegisterNotifier, String>(
                        selector: (context, asset) => asset.sBarcode,
                        builder: (context, barcode, child) {
                          return Text(barcode.toString(),
                              style: TextStyle(
                                  fontFamily: "breuer", fontSize: 12.0));
                        },
                      )),
                ],
              )),
          //======================================================================================================//
          CustomForm(
              context, //Content Form Image
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                    child: Text(
                      "Asset Images",
                      style:
                          TextStyle(fontFamily: "breuer", color: Colors.grey),
                    ),
                  ),
                  CustomDivider(4.0, Colors.black),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Selector<AssetRegisterNotifier, String>(
                                selector: (context, asset) => asset.sImagePath1,
                                builder: (context, image, child) {
                                  return Stack(
                                    children: <Widget>[
                                      fotoPicker(
                                          image.toString(),
                                          "Foto Unit Asset",
                                          screensize.width / 3),
                                      Positioned(
                                        top: -3,
                                        right: -3,
                                        child: GestureDetector(
                                            onTap: () async {
                                              assetRegisNotifier
                                                  .imagePicker("1");
                                            },
                                            child: btnCamera),
                                      )
                                    ],
                                  );
                                }),
                            Selector<AssetRegisterNotifier, String>(
                                selector: (context, asset) => asset.sImagePath2,
                                builder: (context, image, child) {
                                  return Stack(
                                    children: <Widget>[
                                      fotoPicker(
                                          image.toString(),
                                          "Foto Label Barcode",
                                          screensize.width / 3),
                                      Positioned(
                                        top: -3,
                                        right: -3,
                                        child: GestureDetector(
                                            onTap: () async {
                                              assetRegisNotifier
                                                  .imagePicker("2");
                                            },
                                            child: btnCamera),
                                      )
                                    ],
                                  );
                                }),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 1.0),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Selector<AssetRegisterNotifier, String>(
                                selector: (context, asset) => asset.sImagePath3,
                                builder: (context, image, child) {
                                  return Stack(
                                    children: <Widget>[
                                      fotoPicker(
                                          image.toString(),
                                          "Foto Nomor SN",
                                          screensize.width / 3),
                                      Positioned(
                                        top: -3,
                                        right: -3,
                                        child: GestureDetector(
                                            onTap: () async {
                                              assetRegisNotifier
                                                  .imagePicker("3");
                                            },
                                            child: btnCamera),
                                      )
                                    ],
                                  );
                                }),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          //=====================================================================================================//
          CustomForm(
              context,
              Column(
                //Content Form Button
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      ButtonTheme(
                        minWidth: screensize.width / 2.5,
                        child: RaisedButton(
                          onPressed: () async {
                            if (validasiSave(
                                assetRegisNotifier.sBarcode,
                                assetRegisNotifier.sImagePath1,
                                assetRegisNotifier.sImagePath2)) {
                              //Show Loading Bar
                              assetRegisNotifier.loadingShow(true);

                              //Savedata Local
                              final saveValid = await assetRegisNotifier
                                  .saveDataAssetLocally(this._assetID);
                              // Hide Loading Bar
                              assetRegisNotifier.loadingShow(false);

                              //Action Save
                              if (saveValid == false) {
                                Toast.show(
                                    assetRegisNotifier.sErrorMessage.toString(),
                                    context,
                                    duration: Toast.LENGTH_SHORT,
                                    gravity: Toast.BOTTOM);
                              } else {
                                Navigator.of(context).pushNamed(
                                    '/assetregister',
                                    arguments: this._userAccount);

                                assetRegisNotifier.clearAllData();
                              }
                            } else {
                              Toast.show(sErrMsg, context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);
                            }
                          },
                          color: Colors.blue,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.save_alt,
                                color: Colors.white,
                              ),
                              Text(
                                "Save Local",
                                style: TextStyle(
                                    fontFamily: "breuer", color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                      ButtonTheme(
                        minWidth: screensize.width / 2.5,
                        child: RaisedButton(
                          onPressed: () async {
                            if (validasiSave(
                                assetRegisNotifier.sBarcode,
                                assetRegisNotifier.sImagePath1,
                                assetRegisNotifier.sImagePath2)) {
                              //Show Loading Bar
                              assetRegisNotifier.loadingShow(true);

                              //Savedata Local
                              final saveValid = await assetRegisNotifier
                                  .postDataAsset(this._assetID);
                              // Hide Loading Bar
                              assetRegisNotifier.loadingShow(false);

                              //Action Save
                              if (saveValid == false) {
                                Toast.show(
                                    assetRegisNotifier.sErrorMessage.toString(),
                                    context,
                                    duration: Toast.LENGTH_SHORT,
                                    gravity: Toast.BOTTOM);
                              } else {
                                Navigator.of(context).pushNamed(
                                    '/assetregister',
                                    arguments: this._userAccount);

                                //--> Kalo berhasil Upload, Delete Files yang ada di Local Storage.
                                deleteJunkFiles(assetRegisNotifier.filesDelete);
                                assetRegisNotifier.clearAllData();
                              }
                            } else {
                              Toast.show(sErrMsg, context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);
                            }
                          },
                          color: Colors.blue,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.save,
                                color: Colors.white,
                              ),
                              Text(
                                "Upload",
                                style: TextStyle(
                                    fontFamily: "breuer", color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              )),
          //======================================================================================================//
        ]),
        //======================================================================================================//
        Selector<AssetRegisterNotifier, bool>(
            selector: (context, asset) => asset.sHowLoading,
            builder: (context, showLoading, child) {
              return showLoading
                  ? loadingScreen(screensize.height, screensize.width)
                  : loadingScreen(0.0, 0.0);
            }),
      ]),
    );
  }
}
