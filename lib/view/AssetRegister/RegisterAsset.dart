import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jec_asset_mobile/helper/enums/isvalid.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/models/AssetInsertModel.dart';
import 'package:jec_asset_mobile/models/AssetRegisterModel.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/presenter/AssetRegisterPresenter.dart';
import 'package:jec_asset_mobile/view/AssetRegister/IRegisterAsset.dart';
import 'package:toast/toast.dart';

class RegisterAsset extends StatefulWidget {
  final UserAccount _userAccount;
  final String _assetID;
  final AssetRegisterPresenter _presenter;

  RegisterAsset(this._userAccount, this._assetID, this._presenter, {Key key})
      : super(key: key);

  @override
  _RegisterAssetState createState() => _RegisterAssetState();
}

class _RegisterAssetState extends State<RegisterAsset>
    implements Interface_assetRegister {
  AssetRegister model;
  AssetInsert modelInsert;
  String sAssetname, sAssetCode;
  double heightLoading = 0, widthLoading = 0;
  final TextEditingController ctlBarcode = new TextEditingController(text: '');
  String sErrMsg = "";

  @override
  void initState() {
    super.initState();
    this.widget._presenter.view = this;
    print(this.widget._assetID +
        '|' +
        this.widget._userAccount.data.d[0].userId.toString());
  }

  @override
  void refreshPage(AssetRegister _model, AssetInsert _modelInsert,
      IsValid _isValid, List _filesDelete) {
    setState(() {
      this.modelInsert = _modelInsert;
      this.model = _model;

      //--> untuk Close Loading Screen
      if (heightLoading > 0) {
        heightLoading = 0;
        widthLoading = 0;
      }

      //--> Untuk Load Hasil Scan Barcode
      if (_model.barcode != null) ctlBarcode.text = _model.barcode;
    });

    if (_isValid == IsValid.Invalid) {
      Toast.show(
          "Oopss...!! Failed Save data, Failed Connect To Server.", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    } else {
      if (this.modelInsert.result != null) {
        if (this.modelInsert.result == "success") {
          if (this.modelInsert.data[0].pesan == "Success") {
            Navigator.of(context).pushNamed('/assetregister',
                arguments: this.widget._userAccount);

            //--> Kalo berhasil Upload, Delete Files yang ada di Local Storage.
            deleteJunkFiles(_filesDelete);
          } else
            Toast.show("Oopss...!! Failed Post Data to Server.", context,
                duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        } else
          Toast.show("Oopss...!! Failed Save data to Server.", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      }
    }
  }

  bool validasiSave(AssetRegister _model) {
    if (_model.barcode == "" || _model.barcode == null) {
      sErrMsg = "Oopss...!! Barcode Tidak Boleh Kosong.";
      return false;
    } else if (_model.image1 == "" ||
        _model.image1 == null ||
        _model.image2 == "" ||
        _model.image2 == null) {
      sErrMsg =
          "Oopss...!! Foto Unit Asset dan Foto Label Barcode Tidak Boleh Kosong.";
      return false;
    } else
      return true;
  }

  Future imagePicker(String flag) async {
    //Take Image From Camera
    File img = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 15);

    // //Compress Image and Create New File
    if (img != null) {
      setState(() {
        if (flag == '1') {
          model.image1 = img.path;
        } else if (flag == '2') {
          model.image2 = img.path;
        } else if (flag == '3') {
          model.image3 = img.path;
        }
        model.imagepath = img.parent.path.toString();
      });
    }
  }

  var btnCamera = Container(
    alignment: FractionalOffset.center,
    width: 40.0,
    height: 40.0,
    decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.all(const Radius.circular(30.0))),
    child: Icon(
      Icons.add_a_photo,
      color: Colors.white,
      size: 20.0,
    ),
  );

  Widget fotoPicker(
      String _path, String _footer, String _index, double _sizeframe) {
    return Stack(
      key: UniqueKey(),
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
                width: _sizeframe,
                height: _sizeframe,
                decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: _path == "null" || _path == ""
                            ? ExactAssetImage('assets/img/noimage.jpg')
                            : new FileImage(File(_path))))),
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: Text(
                _footer,
                style: TextStyle(
                    fontFamily: "breuer", fontSize: 12.0, color: Colors.grey),
              ),
            )
          ],
        ),
        Positioned(
          top: -3,
          right: -3,
          child: GestureDetector(
              onTap: () {
                imagePicker(_index);
              },
              child: btnCamera),
        )
      ],
    );
  }

  Widget loadingScreen(double _height, double _width) {
    return Container(
      child: Center(
          child: Container(
        decoration: BoxDecoration(
          color: Colors.grey,
        ),
        height: _height / 9,
        width: _width / 1.5,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            Padding(
              padding: const EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
              child: Text(
                "Loading.....   ",
                style: TextStyle(
                    fontFamily: "breuer",
                    fontSize: 20.0,
                    color: Colors.white54),
              ),
            ),
          ],
        ),
      )),
      height: _height,
      width: _width,
      color: Colors.black54,
    );
  }

  Future deleteJunkFiles(List _filesDelete) async {
    //---> Delete Files yang sudah diUpload
    if (_filesDelete.length > 0) {
      //--> Delay dulu, supaya rebuild ui nya selesai.
      await Future.delayed(Duration(seconds: 1));

      //--> Delete Files
      int _index = 0;
      while (_filesDelete.length > _index) {
        if (FileSystemEntity.typeSync(_filesDelete[_index].toString()) !=
            FileSystemEntityType.notFound) {
          File junkfile = new File(_filesDelete[_index].toString());
          junkfile.delete();
        }
        _index++;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final screensize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: CustomAppBarNoHome("Asset Register", context),
      body: Stack(children: <Widget>[
        ListView(physics: ScrollPhysics(), shrinkWrap: true, children: <Widget>[
          //======================================================================================================//
          CustomForm(
              context, //Content Form Barcode Scanning
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                    child: Text(
                      "Asset Barcode",
                      style: TextStyle(
                          fontFamily: "breuer",
                          color: Colors.grey,
                          fontSize: 15.0),
                    ),
                  ),
                  CustomDivider(2.0, Colors.grey),
                  CustomTextField(
                      "Barcode",
                      ctlBarcode,
                      Icons.view_column,
                      Icons.camera_alt,
                      false,
                      this.widget._presenter.scanningQR),
                ],
              )),
          //======================================================================================================//
          CustomForm(
              context, //Content Form Image
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                    child: Text(
                      "Asset Images",
                      style:
                          TextStyle(fontFamily: "breuer", color: Colors.grey),
                    ),
                  ),
                  CustomDivider(4.0, Colors.black),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            fotoPicker(model.image1.toString(),
                                "Foto Unit Asset", "1", screensize.width / 3),
                            fotoPicker(
                                model.image2.toString(),
                                "Foto Label Barcode",
                                "2",
                                screensize.width / 3),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 1.0),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            fotoPicker(
                                model.image3.toString(),
                                "Foto SN (Optional)",
                                "3",
                                screensize.width / 3),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          //=====================================================================================================//
          CustomForm(
              context,
              Column(
                //Content Form Button
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      ButtonTheme(
                        minWidth: screensize.width / 2.5,
                        child: RaisedButton(
                          onPressed: () async {
                            if (validasiSave(this.model)) {
                              setState(() {
                                heightLoading = screensize.height;
                                widthLoading = screensize.width;
                              });

                              final saveValid = await this
                                  .widget
                                  ._presenter
                                  .saveDataAssetLocally(
                                      this.widget._assetID, this.model);

                              setState(() {
                                if (heightLoading > 0) {
                                  heightLoading = 0;
                                  widthLoading = 0;
                                }
                              });

                              if (saveValid == false) {
                                Toast.show(
                                    "Oopss...!! Failed Save data, Failed Connect To Server.",
                                    context,
                                    duration: Toast.LENGTH_SHORT,
                                    gravity: Toast.BOTTOM);
                              } else {
                                this.modelInsert = saveValid;
                                if (this.modelInsert.result == "success") {
                                  if (this.modelInsert.data[0].pesan ==
                                      "Success")
                                    Navigator.of(context).pushNamed(
                                        '/assetregister',
                                        arguments: this.widget._userAccount);
                                  else
                                    Toast.show(
                                        "Oopss...!! Failed Post Data to Server.",
                                        context,
                                        duration: Toast.LENGTH_SHORT,
                                        gravity: Toast.BOTTOM);
                                } else
                                  Toast.show(
                                      "Oopss...!! Failed Save data Locally.",
                                      context,
                                      duration: Toast.LENGTH_SHORT,
                                      gravity: Toast.BOTTOM);
                              }
                            } else {
                              Toast.show(sErrMsg, context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);
                            }
                          },
                          color: Colors.blue,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.save_alt,
                                color: Colors.white,
                              ),
                              Text(
                                "Save Local",
                                style: TextStyle(
                                    fontFamily: "breuer", color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                      ButtonTheme(
                        minWidth: screensize.width / 2.5,
                        child: RaisedButton(
                          onPressed: () async {
                            setState(() {
                              heightLoading = screensize.height;
                              widthLoading = screensize.width;
                            });

                            if (validasiSave(this.model)) {
                              this.widget._presenter.postDataAsset(
                                  this.widget._assetID, this.model);
                            } else {
                              setState(() {
                                if (heightLoading > 0) {
                                  heightLoading = 0;
                                  widthLoading = 0;
                                }
                              });

                              Toast.show(sErrMsg, context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);
                            }
                          },
                          color: Colors.blue,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.save,
                                color: Colors.white,
                              ),
                              Text(
                                "Upload",
                                style: TextStyle(
                                    fontFamily: "breuer", color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              )),
          //======================================================================================================//
        ]),
        //======================================================================================================//
        loadingScreen(heightLoading, widthLoading)
      ]),
    );
  }
}
