import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/models/AssetRegisterListModel.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/presenter/AssetRegisterViewPresenter.dart';
import 'package:jec_asset_mobile/view/AssetRegister/AssetRegisterViewer.dart';

class ProcessList extends StatelessWidget {
  final List<DataAssetRegisterTemp> listData;
  final UserAccount userAccount;

  ProcessList({Key key, @required this.listData, this.userAccount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 5,
          child: listData.length == 0
              ? Center(child: Text("Tidak ada Data."))
              : ListView.builder(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: listData.length,
                  itemBuilder: (BuildContext context, int index) {
                    return CustomFormList(Column(
                      children: <Widget>[
                        ListTile(
                          leading: GestureDetector(
                            child: Icon(
                              Icons.remove_red_eye,
                              color: Colors.blue,
                              size: 30.0,
                            ),
                            onTap: () {
                              Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      // new AssetRegisViewer(this.userAccount,
                                      //     listData[index].id.toString())));
                                      new AssetRegisterViewer(
                                          this.userAccount,
                                          new BasicAssetRegisterViewPresenter(),
                                          listData[index].id.toString())));
                            },
                          ),
                          title: Text(listData[index].assetName,
                              style: TextStyle(
                                  fontFamily: "breuer", fontSize: 13.0),
                              maxLines: 1),
                          subtitle: Text(
                              listData[index].siteName +
                                  ' - ' +
                                  listData[index].roomName,
                              style: TextStyle(
                                  fontFamily: "breuer", fontSize: 11.0),
                              maxLines: 1),
                          trailing: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "SUBMITTED",
                                style: TextStyle(
                                    fontFamily: "breuer",
                                    fontSize: 12.0,
                                    color: Colors.amber),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ));
                  },
                ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            color: Colors.grey[300],
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListView(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text("Status : ",
                          style: TextStyle(
                              fontFamily: "breuer",
                              fontSize: 12.0,
                              color: Colors.grey)),
                      Text("Submitted",
                          style: TextStyle(
                              fontFamily: "breuer",
                              fontSize: 14.0,
                              color: Color(0xffD17600))),
                    ],
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
