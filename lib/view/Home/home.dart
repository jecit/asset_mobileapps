import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:jec_asset_mobile/helper/widgets/customShowDialog.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/helper/widgets/menubar.dart';
import 'package:jec_asset_mobile/models/AssetNotifikasi.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/models/model.dart';
import 'package:jec_asset_mobile/presenter/HomePresenter.dart';
import 'package:jec_asset_mobile/view/Home/IHome.dart';
import 'package:jec_asset_mobile/view/Login/Login.dart';

class Home extends StatefulWidget {
  final UserAccount _userAccount;
  final HomePresenter _presenter;

  const Home(this._userAccount, this._presenter);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> implements Interface_home {
  var statusPageLoad = 0;
  QRData modelQR;
  AssetNotifikasi modelNotifikasi;

  @override
  void initState() {
    super.initState();
    this.widget._presenter.view = this;
    this.widget._presenter.loadListNotifikasi(
        this.widget._userAccount.data.d[0].userId.toString());
  }

  final btnOK = Container(
    alignment: FractionalOffset.center,
    width: 100.0,
    height: 40.0,
    decoration: BoxDecoration(
      color: Colors.blue,
      // borderRadius: BorderRadius.all(const Radius.circular(30.0))
    ),
    child: Text(
      "YES",
      style:
          TextStyle(fontFamily: "breuer", fontSize: 15.0, color: Colors.white),
    ),
  );

  final btnCancel = Container(
    alignment: FractionalOffset.center,
    width: 100.0,
    height: 40.0,
    decoration: BoxDecoration(
      color: Colors.blue,
      // borderRadius: BorderRadius.all(const Radius.circular(30.0))
    ),
    child: Text(
      "NO",
      style:
          TextStyle(fontFamily: "breuer", fontSize: 15.0, color: Colors.white),
    ),
  );

  Future<bool> _popuplogout() async {
    return showDialog(
        context: context,
        builder: (context) => AssetGiffyDialog(
              image: Image.asset(
                'assets/img/GedungJEC_Transparan.png',
                fit: BoxFit.fill,
              ),
              title: Text(
                'Log out',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
              ),
              entryAnimation: EntryAnimation.BOTTOM_LEFT,
              description: Text(
                'Are you sure want to log out?',
                textAlign: TextAlign.center,
                style: TextStyle(),
              ),
              onOkButtonPressed: () => Navigator.of(context).pushNamed('/'),
            )

//            AlertDialog(
//              title: Text(
//                "Are you sure want to log out?",
//                style: TextStyle(fontFamily: "breuer", fontSize: 17.0),
//              ),
//              actions: <Widget>[
//                Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  children: <Widget>[
//                    FlatButton(
//                      child: btnOK,
//                      onPressed: () {
//                        Navigator.of(context).push(new MaterialPageRoute(
//                            builder: (BuildContext context) =>
//                                new LoginPage()));
//                      },
//                    ),
//                    FlatButton(
//                      child: btnCancel,
//                      onPressed: () => Navigator.pop(context, false),
//                    )
//                  ],
//                )
//              ],
//            )
        );
  }

  Widget listitems(_, String _descr, String _subDescr) {
    // final screensize = MediaQuery.of(context).size;
    return Padding(
        padding: EdgeInsets.all(8.0),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 5.0, // has the effect of softening the shadow
                  spreadRadius: 2.0, // has the effect of extending the shadow
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                CustomVerticalDivider(72.0, 1.0, Color(0xffD17600), 5.0),
                Expanded(
                  child: ListTile(
                    leading: Icon(
                      Icons.notifications_active,
                      color: Colors.blue,
                    ),
                    title: Text(
                      _descr,
                      style: TextStyle(fontFamily: "Breuer", fontSize: 16.0),
                    ),
                    subtitle: Text(
                      _subDescr,
                      style: TextStyle(fontFamily: "Breuer", fontSize: 16.0),
                    ),
                  ),
                ),
              ],
            )));
  }

  @override
  Widget build(BuildContext context) {
    var background = Padding(
      padding: EdgeInsets.fromLTRB(5.0, 60.0, 5.0, 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image.asset("assets/img/GedungJEC_Transparan.png"),
          Center(
            child: Text("Copyright @ IT-CORP DIVISION (JEC)"),
          ),
        ],
      ),
    );

    return WillPopScope(
      onWillPop: _popuplogout,
      child: new Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.blue,
            title: new Text('JEC ASSET MANAGEMENT SYSTEM',
                style: TextStyle(fontFamily: "breuer", fontSize: 16.0)),
          ),
          drawer: menuBar(this.widget._userAccount),
          body: Stack(
            children: <Widget>[
              background,
              Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                    Color.fromRGBO(62, 137, 231, 0.5),
                    Color.fromRGBO(251, 251, 251, 0.3)
                  ],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter))),
              this.modelNotifikasi == null ||
                      this.modelNotifikasi.data.length == 0
                  ? SizedBox(
                      width: 0.0,
                      height: 0.0,
                    )
                  : GestureDetector(
                      child: listitems(
                          context,
                          this.modelNotifikasi.data[0].description.toString(),
                          this
                              .modelNotifikasi
                              .data[0]
                              .subDescription
                              .toString()),
                      onTap: () => Navigator.of(context).pushNamed(
                          '/assetregister',
                          arguments: this.widget._userAccount),
                    )
            ],
          )
          // floatingActionButton: new FloatingActionButton.extended(
          //   onPressed: () {
          //     this.widget._presenter.scanningQR();
          //     // this.widget._presenter.delete_file();
          //   },
          //   icon: Icon(Icons.camera_alt),
          //   label: Text(
          //     "Scan Asset",
          //     style: TextStyle(fontFamily: "breuer"),
          //   ),
          // ),
          // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          ),
    );
  }

  @override
  void refreshPage(QRData _model, AssetNotifikasi _modelNotif) {
    setState(() {
      this.modelQR = _model;
      this.modelNotifikasi = _modelNotif;
    });

    if (this.modelQR.bValidScan == true) {
      // Navigator.of(context).push(MaterialPageRoute(
      //     builder: (BuildContext context) => new AssetViewerPage(
      //         this.widget._userAccount,
      //         this.widget.sEmail,
      //         _modelQR.sIDBarcode,
      //         new BasicAssetViewerPresenter(),
      //         true)));
      Center(child: Text(modelQR.sIDBarcode.toString()));
    } else {
      Center(child: Text("Failed Scan, please Try Again..."));
    }
  }
}
