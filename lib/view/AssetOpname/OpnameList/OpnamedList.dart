import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/models/OpnameLIstModel.dart';

class OpnamedList extends StatelessWidget {
  final List<ListOpname> model;

  OpnamedList(this.model);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: model.length,
      physics: ScrollPhysics(),
      itemBuilder: (context, index) {
        return (model.length == 0)
            ? Center(
                child: Text("No Data List Opnamed"),
              )
            : CustomFormList(
                Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(
                        Icons.keyboard_arrow_right,
                        color: Colors.amber,
                        size: 30.0,
                      ),
                      title: Text(
                        model[index].assetname.toString() +
                            " (" +
                            model[index].barcode.toString() +
                            ")",
                      ),
                      subtitle: Text("Opname Date : " +
                          model[index].opnameDate.toString()),
                    ),
                  ],
                ),
              );
      },
    );
  }
}
