import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/loadingscreen.dart';
import 'package:jec_asset_mobile/notifiers/OpnameTransactionNotifier.dart';
import 'package:jec_asset_mobile/view/AssetOpname/OpnameList/AssetListByRoom.dart';
import 'package:jec_asset_mobile/view/AssetOpname/OpnameList/OpnamedList.dart';
import 'package:provider/provider.dart';

class AssetOpnameListByRoom extends StatefulWidget {
  @override
  _AssetOpnameListByRoomState createState() => _AssetOpnameListByRoomState();
}

class _AssetOpnameListByRoomState extends State<AssetOpnameListByRoom>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    Provider.of<AssetOpnameTransactionNotifier>(context, listen: false)
        .fetchDataListOpname();

    _tabController = new TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final transOpnameNotifier =
        Provider.of<AssetOpnameTransactionNotifier>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("List Asset Opname By Room"),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
        bottom: TabBar(
          controller: _tabController,
          tabs: <Widget>[
            new Tab(
              text: "Opnamed List",
            ),
            new Tab(
              text: "Asset List",
            ),
          ],
        ),
      ),
      body: (transOpnameNotifier.listOpname.data == null &&
              transOpnameNotifier.bFetchData == true)
          ? loadingScreen(
              bShow: true,
              sText: "Loading Data...",
            )
          : (transOpnameNotifier.bFetchData == false)
              ? Center(
                  child: Text(
                      "Failed to load Data List Opname, Please try again."),
                )
              : TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    OpnamedList(transOpnameNotifier.listAssetOpnamed),
                    AssetListByRoom(transOpnameNotifier.listAssetByRoom),
                  ],
                ),
    );
  }
}
