import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/helper/widgets/loadingscreen.dart';
import 'package:jec_asset_mobile/notifiers/OpnameTransactionNotifier.dart';
import 'package:jec_asset_mobile/presenter/LoginPresenter.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class AssetOpanemAdd extends StatefulWidget {
  @override
  _AssetOpanemAddState createState() => _AssetOpanemAddState();
}

class _AssetOpanemAddState extends State<AssetOpanemAdd> {
  @override
  void initState() {
    super.initState();
    Provider.of<AssetOpnameTransactionNotifier>(context, listen: false)
        .clearBarcodeList();
  }

  @override
  Widget build(BuildContext context) {
    final screensize = MediaQuery.of(context).size;
    double scannertextsize = screensize.height * 0.25;
    final transNotifier =
        Provider.of<AssetOpnameTransactionNotifier>(context, listen: false);
    final userNotifier = Provider.of<LoginPresenter>(context, listen: false);

    TextEditingController barcodeController =
        new TextEditingController(text: "");
    FocusNode textFocusNode = new FocusNode();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: new Text("Asset Opname"),
        actions: <Widget>[
          InkWell(
            onTap: () async {
              bool uploadValid = await transNotifier.uploadBarcodeList(
                  userNotifier.model.data.d[0].userId.toString());

              if (uploadValid)
                Navigator.of(context).pop();
              else
                Toast.show(
                    "Upload Opname Barcode Failed, Please try again.", context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(Icons.cloud_upload),
            ),
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: scannertextsize,
                child: CustomForm(
                    context,
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                          child: Text(
                            "Asset Barcode",
                            style: TextStyle(
                                fontFamily: "breuer",
                                color: Colors.grey,
                                fontSize: 15.0),
                          ),
                        ),
                        CustomDivider(2.0, Colors.grey),
                        ListTile(
                            leading: Icon(
                              Icons.view_column,
                              color: Colors.blue,
                            ),
                            title: TextFormField(
                              controller: barcodeController,
                              textAlign: TextAlign.right,
                              autofocus: true,
                              focusNode: textFocusNode,
                              autovalidate: true,
                              textInputAction: TextInputAction.done,
                              textCapitalization: TextCapitalization.characters,
                              style: TextStyle(fontSize: 20),
                              keyboardType: TextInputType.number,
                              maxLines: null,
                              onFieldSubmitted: (value) {
                                transNotifier.addBarcodeList(value);
                                FocusScope.of(context)
                                    .requestFocus(textFocusNode);
                              },
                              onChanged: (value) {
                                if (value.contains('\n') || //--> Enter
                                        value.contains('\t') || //--> tab
                                        value.contains('\s') //--> Space
                                    ) {
                                  transNotifier.addBarcodeList(value);
                                  FocusScope.of(context).requestFocus(
                                      textFocusNode); //--> Untuk set autofocus ke textfield
                                }
                                return null;
                              },
                            )),
                      ],
                    )),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Container(
                    height: screensize.height - scannertextsize,
                    child: Consumer<AssetOpnameTransactionNotifier>(
                      builder: (context, opname, _) {
                        return opname.barcodeList.length == 0
                            ? Center(child: Text("No Data Opname"))
                            : ListView.builder(
                                itemCount: opname.barcodeList.length,
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return CustomFormList(Column(
                                    children: <Widget>[
                                      ListTile(
                                        leading: GestureDetector(
                                          child: Icon(
                                            Icons.view_column,
                                            color: Color(0xffD17600),
                                            size: 25.0,
                                          ),
                                          onTap: () {},
                                        ),
                                        title: Text(
                                            opname.barcodeList[index]
                                                .toString(),
                                            style: TextStyle(fontSize: 14.0),
                                            maxLines: 1),
                                      ),
                                    ],
                                  ));
                                },
                              );
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
          Selector<AssetOpnameTransactionNotifier, bool>(
            selector: (context, opname) => opname.bShowLoading,
            builder: (context, showLoading, _) {
              return loadingScreen(
                bShow: showLoading,
                sText: "Uploading...",
              );
            },
          ),
        ],
      ),
    );
  }
}
