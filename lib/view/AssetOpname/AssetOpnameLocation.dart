import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/helper/widgets/loadingscreen.dart';
import 'package:jec_asset_mobile/models/OpnameLocationModel.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/notifiers/OpnameLocationNotifier.dart';
import 'package:jec_asset_mobile/view/AssetOpname/AssetOpnameTransaction.dart';
import 'package:provider/provider.dart';

class AssetOpnameLocation extends StatefulWidget {
  final UserAccount userAccount;

  AssetOpnameLocation({Key key, @required this.userAccount}) : super(key: key);

  @override
  _AssetOpnameLocationState createState() => _AssetOpnameLocationState();
}

class _AssetOpnameLocationState extends State<AssetOpnameLocation> {
  @override
  void initState() {
    super.initState();
    Provider.of<OpnameLocationNotifier>(context, listen: false)
        .getDataOpnameLocation(
            this.widget.userAccount.data.d[0].userId.toString());
  }

  @override
  Widget build(BuildContext context) {
    final screensize = MediaQuery.of(context).size;
    final locationNotifier = Provider.of<OpnameLocationNotifier>(context);

    return Scaffold(
        appBar: CustomAppBar("Asset Opname", context, this.widget.userAccount),
        body: locationNotifier.modelPT.length == 0
            ? loadingScreen(
                bShow: true,
                sText: "Loading...",
              )
            : ListView(
                children: <Widget>[
                  Container(
                    width: screensize.width,
                    child: CustomForm(
                        context,
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                              child: Text(
                                "Asset Opname Location",
                                style: TextStyle(
                                    color: Color(0xff005685), fontSize: 15.0),
                              ),
                            ),
                            CustomDivider(2.0, Colors.grey),
                            // Location PT
                            ListTile(
                                leading: Icon(
                                  Icons.domain,
                                  color: Color(0xffD17600),
                                ),
                                title: Consumer<OpnameLocationNotifier>(
                                  builder: (context, location, child) {
                                    return DropdownButton<OpnameLocation_PT>(
                                      value: location.ptSelected,
                                      items: location.modelPT
                                          .map((pt) => DropdownMenuItem<
                                                  OpnameLocation_PT>(
                                                child: Text(
                                                  pt.ptName,
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "breuer"),
                                                ),
                                                value: pt,
                                              ))
                                          .toList(),
                                      onChanged: (OpnameLocation_PT value) {
                                        location.selectedPT(value);
                                      },
                                      isExpanded: true,
                                      hint: Text(
                                        'Select Company',
                                        style: TextStyle(
                                            fontSize: 14, fontFamily: "breuer"),
                                      ),
                                    );
                                  },
                                )),
                            // Location Site
                            ListTile(
                                leading: Icon(
                                  Icons.domain,
                                  color: Color(0xffD17600),
                                ),
                                title: Consumer<OpnameLocationNotifier>(
                                  builder: (context, location, child) {
                                    return DropdownButton<OpnameLocation_Site>(
                                      value: location.siteSelected,
                                      items: location.modelSite
                                          .map((site) => DropdownMenuItem<
                                                  OpnameLocation_Site>(
                                                child: Text(
                                                  site.site,
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "breuer"),
                                                ),
                                                value: site,
                                              ))
                                          .toList(),
                                      onChanged: (OpnameLocation_Site value) {
                                        location.selectedSite(value);
                                      },
                                      isExpanded: true,
                                      hint: Text(
                                        'Select Site',
                                        style: TextStyle(
                                            fontSize: 14, fontFamily: "breuer"),
                                      ),
                                    );
                                  },
                                )),
                            //Location Floor
                            ListTile(
                                leading: Icon(
                                  Icons.location_on,
                                  color: Color(0xffD17600),
                                ),
                                title: Consumer<OpnameLocationNotifier>(
                                  builder: (context, location, _) {
                                    return DropdownButton<OpnameLocation_Floor>(
                                      value: location.floorSelected,
                                      items: location.modelFloor
                                          .map((floor) => DropdownMenuItem<
                                                  OpnameLocation_Floor>(
                                                child: Text(
                                                  floor.floorDesc,
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "breuer"),
                                                ),
                                                value: floor,
                                              ))
                                          .toList(),
                                      onChanged: (OpnameLocation_Floor value) {
                                        location.selectedFloor(value);
                                      },
                                      isExpanded: true,
                                      hint: Text('Select Floor',
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: "breuer")),
                                    );
                                  },
                                )),
                          ],
                        )),
                  ),

                  // Room Lists
                  Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 20.0, 0.0, 5.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Room Lists",
                            style: TextStyle(
                                color: Color(0xff005685), fontSize: 15.0),
                          ),
                        ],
                      )),
                  CustomDivider(2.0, Color(0xff005685)),

                  Consumer<OpnameLocationNotifier>(
                    builder: (context, location, _) {
                      return location.modelRoom.length == 0
                          ? Center(child: Text("No Data Room"))
                          : Container(
                              height: screensize.height / 2,
                              child: ListView.builder(
                                itemCount: location.modelRoom.length,
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return CustomFormList(Column(
                                    children: <Widget>[
                                      ListTile(
                                        leading: GestureDetector(
                                          child: Icon(
                                            Icons.location_on,
                                            color: Color(0xffD17600),
                                            size: 25.0,
                                          ),
                                        ),
                                        title: Text(
                                            location.modelRoom[index].roomName,
                                            style: TextStyle(fontSize: 14.0),
                                            maxLines: 1),
                                        subtitle: Row(
                                          children: <Widget>[
                                            Text("Status : ",
                                                style: TextStyle(
                                                  fontSize: 13.0,
                                                ),
                                                maxLines: 1),
                                            Text(
                                              location.modelRoom[index]
                                                  .statusOpname,
                                              style: TextStyle(
                                                  fontSize: 13.0,
                                                  color: location
                                                              .modelRoom[index]
                                                              .statusOpname ==
                                                          'Closed'
                                                      ? Colors.green
                                                      : location
                                                                  .modelRoom[
                                                                      index]
                                                                  .statusOpname ==
                                                              'Process'
                                                          ? Colors.amber
                                                          : Colors.blue),
                                            )
                                          ],
                                        ),
                                        trailing: GestureDetector(
                                          child: location.modelRoom[index]
                                                      .statusOpname ==
                                                  'Closed'
                                              ? Icon(
                                                  Icons.lock,
                                                  color: Colors.green,
                                                  size: 30.0,
                                                )
                                              : Icon(
                                                  Icons.add_circle,
                                                  color: location
                                                              .modelRoom[index]
                                                              .statusOpname ==
                                                          'Process'
                                                      ? Colors.amber
                                                      : Colors.blue,
                                                  size: 30.0,
                                                ),
                                          onTap: () async {
                                            location.selectedRoom(
                                                location.modelRoom[index]);
                                            Navigator.of(context).pushNamed(
                                                '/assetopname/list',
                                                arguments: location
                                                    .modelRoom[index]
                                                    .statusOpname
                                                    .toString());
                                          },
                                        ),
                                      ),
                                    ],
                                  ));
                                },
                              ),
                            );
                    },
                  ),
                ],
              ));
  }
}
