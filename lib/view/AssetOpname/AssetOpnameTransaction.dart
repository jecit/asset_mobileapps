import 'package:flutter/material.dart';
import 'package:jec_asset_mobile/helper/widgets/customwidget.dart';
import 'package:jec_asset_mobile/helper/widgets/loadingscreen.dart';
import 'package:jec_asset_mobile/models/UserAccount.dart';
import 'package:jec_asset_mobile/notifiers/OpnameLocationNotifier.dart';
import 'package:jec_asset_mobile/notifiers/OpnameTransactionNotifier.dart';
import 'package:jec_asset_mobile/presenter/LoginPresenter.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class AssetOpnametransaction extends StatefulWidget {
  final String sStatusRoom;

  AssetOpnametransaction({Key key, @required this.sStatusRoom})
      : super(key: key);

  @override
  _AssetOpnametransactionState createState() => _AssetOpnametransactionState();
}

class _AssetOpnametransactionState extends State<AssetOpnametransaction> {
  @override
  void initState() {
    print(this.widget.sStatusRoom.toString());

    final location =
        Provider.of<OpnameLocationNotifier>(context, listen: false);
    final user = Provider.of<LoginPresenter>(context, listen: false);
    Provider.of<AssetOpnameTransactionNotifier>(context, listen: false)
        .getOpnameID(
      location.siteSelected.siteId,
      location.roomSelected.roomId,
      user.model.data.d[0].userId.toString(),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final locationNotifier = Provider.of<OpnameLocationNotifier>(context);
    final transOpnameNotifier =
        Provider.of<AssetOpnameTransactionNotifier>(context);
    final userNotifier = Provider.of<LoginPresenter>(context, listen: false);

    var btnCloseRoom = RaisedButton(
      onPressed: () async {
        bool _closedRoom = await transOpnameNotifier.closeOpnameRoom();

        if (_closedRoom)
          Navigator.of(context)
              .pushNamed('/assetopname', arguments: userNotifier.model);
        else
          Toast.show(
              "Closing Room Process failed, Please try again later.", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      },
      color: Colors.blue,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(10.0),
        side: BorderSide(color: Colors.blueGrey),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.lock,
            color: Colors.white,
          ),
          SizedBox(width: 8.0),
          Text(
            "Close Room",
            style: TextStyle(color: Colors.white, fontSize: 15.0),
          ),
        ],
      ),
    );

    var btnListAssetRoom = RaisedButton(
      onPressed: () =>
          Navigator.of(context).pushNamed('/assetopname/listasset'),
      color: Colors.blue,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(10.0),
        side: BorderSide(color: Colors.blueGrey),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.list,
            color: Colors.white,
          ),
          SizedBox(width: 8.0),
          Text(
            "List Opname",
            style: TextStyle(color: Colors.white, fontSize: 15.0),
          ),
        ],
      ),
    );

    print(transOpnameNotifier.headeropnameid.data);

    return Scaffold(
        appBar: AppBar(
          leading: GestureDetector(
            child: Icon(Icons.arrow_back),
            onTap: () {
              Provider.of<AssetOpnameTransactionNotifier>(context,
                      listen: false)
                  .clearHeaderopnameid();
              Navigator.of(context).pop();
            },
          ),
          backgroundColor: Colors.blue,
          centerTitle: true,
          title: Padding(
            padding: EdgeInsets.only(left: 40.0),
            child: new Text(
              "Asset Opname Room",
              style: TextStyle(fontFamily: "breuer"),
            ),
          ),
        ),
        body: (transOpnameNotifier.headeropnameid.data == null)
            ? loadingScreen(
                bShow: true,
                sText: "Loading...",
              )
            : Stack(
                children: <Widget>[
                  ListView(
                    physics: ScrollPhysics(),
                    children: <Widget>[
                      Container(
                        child: CustomForm(
                          context,
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    EdgeInsets.fromLTRB(20.0, 8.0, 0.0, 5.0),
                                child: Text(
                                  "Asset Location",
                                  style: TextStyle(
                                      fontFamily: "breuer",
                                      color: Colors.black87,
                                      fontSize: 15.0),
                                ),
                              ),
                              CustomDivider(2.0, Colors.grey),
                              CustommTextView("Asset Company : ",
                                  locationNotifier.ptSelected.ptName),
                              CustomDivider(2.0, Colors.grey),
                              CustommTextView("Asset Site : ",
                                  locationNotifier.siteSelected.site),
                              CustomDivider(2.0, Colors.grey),
                              CustommTextView("Asset Room : ",
                                  locationNotifier.roomSelected.roomName),
                              CustomDivider(2.0, Colors.grey),
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Text(
                                  "Room Status",
                                  style: TextStyle(
                                      color: Colors.blue[300],
                                      fontSize: 10.0,
                                      fontFamily: "breuer"),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 1.0),
                              ),
                              Center(
                                child: Text(
                                  this.widget.sStatusRoom,
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      color: this.widget.sStatusRoom == "Idle"
                                          ? Colors.blue
                                          : this.widget.sStatusRoom == "Process"
                                              ? Colors.amber
                                              : Colors.green),
                                ),
                              ),
                              CustomDivider(2.0, Colors.grey),
                            ],
                          ),
                        ),
                      ),
                      CustomForm(
                          context,
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              this.widget.sStatusRoom == "Closed"
                                  ? Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: btnListAssetRoom,
                                      ),
                                    )
                                  : Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        btnCloseRoom,
                                        btnListAssetRoom,
                                      ],
                                    )
                            ],
                          )),
                    ],
                  ),
                  Selector<AssetOpnameTransactionNotifier, bool>(
                    selector: (context, opname) => opname.bShowLoading,
                    builder: (context, showLoading, _) {
                      return loadingScreen(
                        bShow: showLoading,
                        sText: "Closing Room....",
                      );
                    },
                  ),
                ],
              ),
        floatingActionButton: this.widget.sStatusRoom == "Closed"
            ? SizedBox(
                height: 0.0,
                width: 0.0,
              )
            : FloatingActionButton.extended(
                icon: Icon(
                  Icons.add_circle_outline,
                  color: Colors.white,
                  size: 25.0,
                ),
                label: Text(
                  "Start Opname",
                  style: TextStyle(fontFamily: "breuer", fontSize: 14.0),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed('/assetopname/add');
                },
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat);
  }
}
