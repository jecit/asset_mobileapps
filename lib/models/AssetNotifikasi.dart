// To parse this JSON data, do
//
//     final assetNotifikasi = assetNotifikasiFromJson(jsonString);

import 'dart:convert';

AssetNotifikasi assetNotifikasiFromJson(String str) =>
    AssetNotifikasi.fromJson(json.decode(str));

String assetNotifikasiToJson(AssetNotifikasi data) =>
    json.encode(data.toJson());

class AssetNotifikasi {
  String result;
  List<DataNotifikasi> data;

  AssetNotifikasi({
    this.result,
    this.data,
  });

  factory AssetNotifikasi.fromJson(Map<String, dynamic> json) =>
      AssetNotifikasi(
        result: json["result"],
        data: List<DataNotifikasi>.from(
            json["data"].map((x) => DataNotifikasi.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataNotifikasi {
  String description;
  String subDescription;
  String userid;

  DataNotifikasi({
    this.description,
    this.subDescription,
    this.userid,
  });

  factory DataNotifikasi.fromJson(Map<String, dynamic> json) => DataNotifikasi(
        description: json["Description"],
        subDescription: json["SubDescription"],
        userid: json["userid"],
      );

  Map<String, dynamic> toJson() => {
        "Description": description,
        "SubDescription": subDescription,
        "userid": userid,
      };
}
