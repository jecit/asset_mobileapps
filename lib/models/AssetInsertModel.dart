// To parse this JSON data, do
//
//     final assetInsert = assetInsertFromJson(jsonString);

import 'dart:convert';

AssetInsert assetInsertFromJson(String str) =>
    AssetInsert.fromJson(json.decode(str));

String assetInsertToJson(AssetInsert data) => json.encode(data.toJson());

class AssetInsert {
  String result;
  List<Datum> data;

  AssetInsert({
    this.result,
    this.data,
  });

  factory AssetInsert.fromJson(Map<String, dynamic> json) => new AssetInsert(
        result: json["result"],
        data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int assetid;
  String pesan;

  Datum({
    this.assetid,
    this.pesan,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
        assetid: json["assetid"],
        pesan: json["Pesan"],
      );

  Map<String, dynamic> toJson() => {
        "assetid": assetid,
        "Pesan": pesan,
      };
}
