// To parse this JSON data, do
//
//     final assetSearch = assetSearchFromJson(jsonString);

import 'dart:convert';

AssetSearch assetSearchFromJson(String str) => AssetSearch.fromJson(json.decode(str));

String assetSearchToJson(AssetSearch data) => json.encode(data.toJson());

class AssetSearch {
    String result;
    List<DataAssetSearch> data;

    AssetSearch({
        this.result,
        this.data,
    });

    factory AssetSearch.fromJson(Map<String, dynamic> json) => new AssetSearch(
        result: json["result"],
        data: new List<DataAssetSearch>.from(json["data"].map((x) => DataAssetSearch.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "result": result,
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class DataAssetSearch {
    int assetTypeId;
    String assetType;
    int assetGroupId;
    String assetGroup;

    DataAssetSearch({
        this.assetTypeId,
        this.assetType,
        this.assetGroupId,
        this.assetGroup,
    });

    factory DataAssetSearch.fromJson(Map<String, dynamic> json) => new DataAssetSearch(
        assetTypeId: json["AssetTypeID"],
        assetType: json["AssetType"],
        assetGroupId: json["AssetGroupID"],
        assetGroup: json["AssetGroup"],
    );

    Map<String, dynamic> toJson() => {
        "AssetTypeID": assetTypeId,
        "AssetType": assetType,
        "AssetGroupID": assetGroupId,
        "AssetGroup": assetGroup,
    };
}
