// To parse this JSON data, do
//
//     final globalModelGetId = globalModelGetIdFromJson(jsonString);

import 'dart:convert';

GlobalModelGetId globalModelGetIdFromJson(String str) =>
    GlobalModelGetId.fromJson(json.decode(str));

String globalModelGetIdToJson(GlobalModelGetId data) =>
    json.encode(data.toJson());

class GlobalModelGetId {
  String result;
  List<DataID> data;

  GlobalModelGetId({
    this.result,
    this.data,
  });

  factory GlobalModelGetId.fromJson(Map<String, dynamic> json) =>
      GlobalModelGetId(
        result: json["result"],
        data: List<DataID>.from(json["data"].map((x) => DataID.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataID {
  int id;

  DataID({
    this.id,
  });

  factory DataID.fromJson(Map<String, dynamic> json) => DataID(
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
      };
}
