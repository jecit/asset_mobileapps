// To parse this JSON data, do
//
//     final modifiedResponseModel = modifiedResponseModelFromJson(jsonString);

import 'dart:convert';

ModifiedResponseModel modifiedResponseModelFromJson(String str) =>
    ModifiedResponseModel.fromJson(json.decode(str));

String modifiedResponseModelToJson(ModifiedResponseModel data) =>
    json.encode(data.toJson());

class ModifiedResponseModel {
  String result;
  List<DataResponse> data;

  ModifiedResponseModel({
    this.result,
    this.data,
  });

  factory ModifiedResponseModel.fromJson(Map<String, dynamic> json) =>
      ModifiedResponseModel(
        result: json["result"],
        data: List<DataResponse>.from(
            json["data"].map((x) => DataResponse.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataResponse {
  String pesan;

  DataResponse({
    this.pesan,
  });

  factory DataResponse.fromJson(Map<String, dynamic> json) => DataResponse(
        pesan: json["pesan"],
      );

  Map<String, dynamic> toJson() => {
        "pesan": pesan,
      };
}
