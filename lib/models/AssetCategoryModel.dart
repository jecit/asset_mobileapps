// To parse this JSON data, do
//
//     final category = categoryFromJson(jsonString);

import 'dart:convert';

AssetCategory categoryFromJson(String str) =>
    AssetCategory.fromJson(json.decode(str));

String categoryToJson(AssetCategory data) => json.encode(data.toJson());

class AssetCategory {
  String result;
  List<DataCategory> data;
  DataCategory categorySelected;

  AssetCategory({
    this.result,
    this.data,
    this.categorySelected,
  });

  factory AssetCategory.fromJson(Map<String, dynamic> json) =>
      new AssetCategory(
        result: json["result"],
        data: new List<DataCategory>.from(
            json["data"].map((x) => DataCategory.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataCategory {
  int categoryId;
  String categoryName;

  DataCategory({
    this.categoryId,
    this.categoryName,
  });

  factory DataCategory.fromJson(Map<String, dynamic> json) => new DataCategory(
        categoryId: json["CategoryID"],
        categoryName: json["CategoryName"],
      );

  Map<String, dynamic> toJson() => {
        "CategoryID": categoryId,
        "CategoryName": categoryName,
      };

  //--> Untuk Pengecekkan instance(hashcode) pada object. (untuk handle FutureBuilder)
  bool operator ==(o) => o is DataCategory && o.categoryId == categoryId;
  int get hashCode => categoryId.hashCode;
}
