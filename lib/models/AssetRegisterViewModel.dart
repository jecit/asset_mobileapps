// To parse this JSON data, do
//
//     final assetRegisterView = assetRegisterViewFromJson(jsonString);

import 'dart:convert';

AssetRegisterView assetRegisterViewFromJson(String str) =>
    AssetRegisterView.fromJson(json.decode(str));

String assetRegisterViewToJson(AssetRegisterView data) =>
    json.encode(data.toJson());

class AssetRegisterView {
  String result;
  List<DataAssetView> data;

  AssetRegisterView({
    this.result,
    this.data,
  });

  factory AssetRegisterView.fromJson(Map<String, dynamic> json) =>
      new AssetRegisterView(
        result: json["result"],
        data: new List<DataAssetView>.from(
            json["data"].map((x) => DataAssetView.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataAssetView {
  int id;
  String assetCode;
  String assetName;
  String assetBarcode;
  String ptName;
  String siteName;
  String roomName;
  String assetGroup;
  String assetSubGroup;
  String assetType;
  String image1;
  String image2;
  String image3;
  String status;
  String alasanReject;
  String imagepath;

  DataAssetView(
      {this.id,
      this.assetCode,
      this.assetName,
      this.assetBarcode,
      this.ptName,
      this.siteName,
      this.roomName,
      this.assetGroup,
      this.assetSubGroup,
      this.assetType,
      this.image1,
      this.image2,
      this.image3,
      this.status,
      this.alasanReject,
      this.imagepath});

  factory DataAssetView.fromJson(Map<String, dynamic> json) =>
      new DataAssetView(
        id: json["ID"],
        assetCode: json["AssetCode"],
        assetName: json["AssetName"],
        assetBarcode: json["AssetBarcode"],
        ptName: json["PTName"],
        siteName: json["SiteName"],
        roomName: json["RoomName"],
        assetGroup: json["AssetGroup"],
        assetSubGroup: json["AssetSubGroup"],
        assetType: json["AssetType"],
        image1: json["image1"],
        image2: json["image2"],
        image3: json["image3"],
        status: json["Status"],
        alasanReject: json["AlasanReject"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "AssetCode": assetCode,
        "AssetName": assetName,
        "AssetBarcode": assetBarcode,
        "PTName": ptName,
        "SiteName": siteName,
        "RoomName": roomName,
        "AssetGroup": assetGroup,
        "AssetSubGroup": assetSubGroup,
        "AssetType": assetType,
        "image1": image1,
        "image2": image2,
        "image3": image3,
        "Status": status,
        "AlasanReject": alasanReject,
        "ImagePath": imagepath
      };

  Map<String, dynamic> toJsonImage(String sImg1, String sImg2, String sImg3) =>
      {
        "barcode": assetBarcode,
        "image1": sImg1,
        "image2": sImg2,
        "image3": sImg3,
        "imagepath": imagepath,
      };
}
