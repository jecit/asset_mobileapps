// To parse this JSON data, do
//
//     final assetLocation = assetLocationFromJson(jsonString);

import 'dart:convert';

AssetLocationModel assetLocationFromJson(String str) =>
    AssetLocationModel.fromJson(json.decode(str));

String assetLocationToJson(AssetLocationModel data) =>
    json.encode(data.toJson());

class AssetLocationModel {
  String result;
  List<DataLocation> data;
  DataLocationPT assetPTSelected;
  DataLocationSite assetSiteSelected;
  DataLocationFloor assetFloorSelected;
  DataLocationRoom assetRoomSelected;

  AssetLocationModel({
    this.result,
    this.data,
  });

  factory AssetLocationModel.fromJson(Map<String, dynamic> json) =>
      new AssetLocationModel(
        result: json["result"],
        data: new List<DataLocation>.from(
            json["data"].map((x) => DataLocation.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataLocation {
  int ptid;
  String ptName;
  String siteId;
  String site;
  String floorLevel;
  String floorDesc;
  String siteRoomId;
  String siteRoomName;

  DataLocation({
    this.ptid,
    this.ptName,
    this.siteId,
    this.site,
    this.floorLevel,
    this.floorDesc,
    this.siteRoomId,
    this.siteRoomName,
  });

  factory DataLocation.fromJson(Map<String, dynamic> json) => new DataLocation(
        ptid: json["PTID"],
        ptName: json["PTName"],
        siteId: json["SiteID"],
        site: json["Site"],
        floorLevel: json["FloorLevel"],
        floorDesc: json["FloorDesc"],
        siteRoomId: json["SiteRoomID"],
        siteRoomName: json["SiteRoomName"],
      );

  Map<String, dynamic> toJson() => {
        "PTID": ptid,
        "PTName": ptName,
        "SiteID": siteId,
        "Site": site,
        "FloorLevel": floorLevel,
        "FloorDesc": floorDesc,
        "SiteRoomID": siteRoomId,
        "SiteRoomName": siteRoomName,
      };
}

class DataLocationPT {
  int ptid;
  String ptName;

  DataLocationPT({
    this.ptid,
    this.ptName,
  });

  factory DataLocationPT.fromLocation(DataLocation location) =>
      new DataLocationPT(ptid: location.ptid, ptName: location.ptName);
}

class DataLocationSite {
  int ptid;
  String siteId;
  String site;

  DataLocationSite({
    this.ptid,
    this.siteId,
    this.site,
  });

  factory DataLocationSite.fromLocation(DataLocation location) =>
      new DataLocationSite(
          ptid: location.ptid, siteId: location.siteId, site: location.site);
}

class DataLocationFloor {
  String siteId;
  String floorLevel;
  String floorDesc;

  DataLocationFloor({
    this.siteId,
    this.floorLevel,
    this.floorDesc,
  });

  factory DataLocationFloor.fromLocation(DataLocation location) =>
      new DataLocationFloor(
          siteId: location.siteId,
          floorLevel: location.floorLevel,
          floorDesc: location.floorDesc);
}

class DataLocationRoom {
  String siteId;
  String floorLevel;
  String siteRoomId;
  String siteRoomName;

  DataLocationRoom({
    this.siteId,
    this.floorLevel,
    this.siteRoomId,
    this.siteRoomName,
  });

  factory DataLocationRoom.fromLocation(DataLocation location) =>
      new DataLocationRoom(
          siteId: location.siteId,
          floorLevel: location.floorLevel,
          siteRoomId: location.siteRoomId,
          siteRoomName: location.siteRoomName);
}
