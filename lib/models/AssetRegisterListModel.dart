// To parse this JSON data, do
//
//     final AssetRegisterListModel = AssetRegisterListModelFromJson(jsonString);

import 'dart:convert';

AssetRegisterListModel AssetRegisterListModelFromJson(String str) =>
    AssetRegisterListModel.fromJson(json.decode(str));

String AssetRegisterListModelToJson(AssetRegisterListModel data) =>
    json.encode(data.toJson());

class AssetRegisterListModel {
  String result;
  List<DataAssetRegister> data;

  AssetRegisterListModel({
    this.result,
    this.data,
  });

  factory AssetRegisterListModel.fromJson(Map<String, dynamic> json) =>
      new AssetRegisterListModel(
        result: json["result"],
        data: new List<DataAssetRegister>.from(
            json["data"].map((x) => DataAssetRegister.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataAssetRegister {
  int id;
  String assetName;
  String assetBarcode;
  String siteName;
  String roomName;
  String status;

  DataAssetRegister({
    this.id,
    this.assetName,
    this.assetBarcode,
    this.siteName,
    this.roomName,
    this.status,
  });

  factory DataAssetRegister.fromJson(Map<String, dynamic> json) =>
      new DataAssetRegister(
        id: json["ID"],
        assetName: json["AssetName"],
        assetBarcode: json["AssetBarcode"],
        siteName: json["SiteName"],
        roomName: json["RoomName"],
        status: json["Status"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "AssetName": assetName,
        "AssetBarcode": assetBarcode,
        "SiteName": siteName,
        "RoomName": roomName,
        "Status": status,
      };
}

class DataAssetRegisterTemp {
  int id;
  String assetName;
  String assetBarcode;
  String siteName;
  String roomName;
  String status;

  DataAssetRegisterTemp({
    this.id,
    this.assetName,
    this.assetBarcode,
    this.siteName,
    this.roomName,
    this.status,
  });

  factory DataAssetRegisterTemp.fromAssetRegister(DataAssetRegister value) =>
      new DataAssetRegisterTemp(
          id: value.id,
          assetName: value.assetName,
          assetBarcode: value.assetBarcode,
          siteName: value.siteName,
          roomName: value.roomName,
          status: value.status);
}
