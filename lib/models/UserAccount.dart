// To parse this JSON data, do
//
//     final userAccount = userAccountFromJson(jsonString);

import 'dart:convert';

UserAccount userAccountFromJson(String str) =>
    UserAccount.fromJson(json.decode(str));

String userAccountToJson(UserAccount data) => json.encode(data.toJson());

class UserAccount {
  String result;
  Data data;

  UserAccount({
    this.result,
    this.data,
  });

  factory UserAccount.fromJson(Map<String, dynamic> json) => new UserAccount(
        result: json["result"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": data.toJson(),
      };

  bool operator ==(o) => o is UserAccount && o.data == data;
  int get hashCode => data.hashCode;
}

class Data {
  List<D> d;

  Data({
    this.d,
  });

  factory Data.fromJson(Map<String, dynamic> json) => new Data(
        d: new List<D>.from(json["d"].map((x) => D.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "d": new List<dynamic>.from(d.map((x) => x.toJson())),
      };
}

class D {
  String type;
  String userId;
  String userName;
  String password;
  String email;
  String status;
  String pesan;

  D({
    this.type,
    this.userId,
    this.userName,
    this.password,
    this.email,
    this.status,
    this.pesan,
  });

  factory D.fromJson(Map<String, dynamic> json) => new D(
        type: json["__type"],
        userId: json["UserID"],
        userName: json["UserName"],
        password: json["Password"],
        email: json["Email"],
        status: json["Status"],
        pesan: json["Pesan"],
      );

  Map<String, dynamic> toJson() => {
        "__type": type,
        "UserID": userId,
        "UserName": userName,
        "Password": password,
        "Email": email,
        "Status": status,
        "Pesan": pesan,
      };
  bool operator ==(o) => o is D && o.userId == userId;
  int get hashCode => userId.hashCode;
}

class AssetRole {
  String result;
  List<DataAssetRole> data;

  AssetRole({
    this.result,
    this.data,
  });

  factory AssetRole.fromJson(Map<String, dynamic> json) => new AssetRole(
        result: json["result"],
        data: new List<DataAssetRole>.from(
            json["data"].map((x) => DataAssetRole.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataAssetRole {
  int ptid;
  String ptName;

  DataAssetRole({
    this.ptid,
    this.ptName,
  });

  factory DataAssetRole.fromJson(Map<String, dynamic> json) =>
      new DataAssetRole(
        ptid: json["PTID"],
        ptName: json["PTName"],
      );

  Map<String, dynamic> toJson() => {
        "PTID": ptid,
        "PTName": ptName,
      };
}
