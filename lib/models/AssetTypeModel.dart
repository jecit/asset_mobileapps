// To parse this JSON data, do
//
//     final assetType = assetTypeFromJson(jsonString);

import 'dart:convert';

AssetType assetTypeFromJson(String str) => AssetType.fromJson(json.decode(str));

String assetTypeToJson(AssetType data) => json.encode(data.toJson());

class AssetType {
  String result;
  List<DataAssetType> data;
  DataAssetType assetTypeSelected;

  AssetType({this.result, this.data, this.assetTypeSelected});

  factory AssetType.fromJson(Map<String, dynamic> json) => new AssetType(
        result: json["result"],
        data: new List<DataAssetType>.from(
            json["data"].map((x) => DataAssetType.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataAssetType {
  int assetTypeId;
  String assetType;
  int assetSubGroupId;
  String assetSubGroup;
  int assetGroupId;
  String assetGroup;

  DataAssetType({
    this.assetTypeId,
    this.assetType,
    this.assetSubGroupId,
    this.assetSubGroup,
    this.assetGroupId,
    this.assetGroup,
  });

  factory DataAssetType.fromJson(Map<String, dynamic> json) =>
      new DataAssetType(
        assetTypeId: json["AssetTypeID"],
        assetType: json["AssetType"],
        assetSubGroupId: json["AssetSubGroupID"],
        assetSubGroup: json["AssetSubGroup"],
        assetGroupId: json["AssetGroupID"],
        assetGroup: json["AssetGroup"],
      );

  Map<String, dynamic> toJson() => {
        "AssetTypeID": assetTypeId,
        "AssetType": assetType,
        "AssetSubGroupID": assetSubGroupId,
        "AssetSubGroup": assetSubGroup,
        "AssetGroupID": assetGroupId,
        "AssetGroup": assetGroup,
      };
}
