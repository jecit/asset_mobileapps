// To parse this JSON data, do
//
//     final getFileNameUpload = getFileNameUploadFromJson(jsonString);

import 'dart:convert';

GetFileNameUpload getFileNameUploadFromJson(String str) =>
    GetFileNameUpload.fromJson(json.decode(str));

String getFileNameUploadToJson(GetFileNameUpload data) =>
    json.encode(data.toJson());

class GetFileNameUpload {
  String result;
  Data data;

  GetFileNameUpload({
    this.result,
    this.data,
  });

  factory GetFileNameUpload.fromJson(Map<String, dynamic> json) =>
      new GetFileNameUpload(
        result: json["result"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": data.toJson(),
      };
}

class Data {
  List<Datum> data;
  List<FileElement> files;

  Data({
    this.data,
    this.files,
  });

  factory Data.fromJson(Map<String, dynamic> json) => new Data(
        data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        files: new List<FileElement>.from(
            json["files"].map((x) => FileElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
        "files": new List<dynamic>.from(files.map((x) => x.toJson())),
      };
}

class Datum {
  String pesan;

  Datum({
    this.pesan,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
        pesan: json["Pesan"],
      );

  Map<String, dynamic> toJson() => {
        "Pesan": pesan,
      };
}

class FileElement {
  String id;

  FileElement({
    this.id,
  });

  factory FileElement.fromJson(Map<String, dynamic> json) => new FileElement(
        id: json["ID"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
      };
}
