// To parse this JSON data, do
//
//     final opnameLocationModel = opnameLocationModelFromJson(jsonString);

import 'dart:convert';

OpnameLocationModel opnameLocationModelFromJson(String str) =>
    OpnameLocationModel.fromJson(json.decode(str));

String opnameLocationModelToJson(OpnameLocationModel data) =>
    json.encode(data.toJson());

class OpnameLocationModel {
  String result;
  List<DataOpnameLocation> data;

  OpnameLocationModel({
    this.result,
    this.data,
  });

  factory OpnameLocationModel.fromJson(Map<String, dynamic> json) =>
      OpnameLocationModel(
        result: json["result"],
        data: List<DataOpnameLocation>.from(
            json["data"].map((x) => DataOpnameLocation.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataOpnameLocation {
  int ptid;
  String ptName;
  String siteId;
  String site;
  String floorLevel;
  String floorDesc;
  String siteRoomId;
  String siteRoomName;
  String statusOpname;

  DataOpnameLocation({
    this.ptid,
    this.ptName,
    this.siteId,
    this.site,
    this.floorLevel,
    this.floorDesc,
    this.siteRoomId,
    this.siteRoomName,
    this.statusOpname,
  });

  factory DataOpnameLocation.fromJson(Map<String, dynamic> json) =>
      DataOpnameLocation(
        ptid: json["PTID"],
        ptName: json["PTName"],
        siteId: json["SiteID"],
        site: json["Site"],
        floorLevel: json["FloorLevel"],
        floorDesc: json["FloorDesc"],
        siteRoomId: json["SiteRoomID"],
        siteRoomName: json["SiteRoomName"],
        statusOpname: json["StatusOpname"],
      );

  Map<String, dynamic> toJson() => {
        "PTID": ptid,
        "PTName": ptName,
        "SiteID": siteId,
        "Site": site,
        "FloorLevel": floorLevel,
        "FloorDesc": floorDesc,
        "SiteRoomID": siteRoomId,
        "SiteRoomName": siteRoomName,
        "StatusOpname": statusOpname,
      };
}

class OpnameLocation_PT {
  int ptid;
  String ptName;

  OpnameLocation_PT({
    this.ptid,
    this.ptName,
  });

  factory OpnameLocation_PT.fromOpnameLocation(DataOpnameLocation location) =>
      OpnameLocation_PT(ptid: location.ptid, ptName: location.ptName);

  factory OpnameLocation_PT.addValue(OpnameLocation_PT pt) =>
      OpnameLocation_PT(ptid: pt.ptid, ptName: pt.ptName);
}

class OpnameLocation_Site {
  int sitePTid;
  String siteId;
  String site;

  OpnameLocation_Site({this.sitePTid, this.siteId, this.site});

  factory OpnameLocation_Site.fromOpnameLocation(DataOpnameLocation location) =>
      OpnameLocation_Site(
          sitePTid: location.ptid,
          siteId: location.siteId,
          site: location.site);
}

class OpnameLocation_Floor {
  String floorSiteID;
  String floorLevel;
  String floorDesc;

  OpnameLocation_Floor({
    this.floorSiteID,
    this.floorLevel,
    this.floorDesc,
  });

  factory OpnameLocation_Floor.fromOpnameLocation(
          DataOpnameLocation location) =>
      OpnameLocation_Floor(
          floorSiteID: location.siteId,
          floorLevel: location.floorLevel,
          floorDesc: location.floorDesc);
}

class OpnameLocation_Room {
  String roomSiteID, roomId, roomName, statusOpname;

  OpnameLocation_Room({
    this.roomSiteID,
    this.roomId,
    this.roomName,
    this.statusOpname,
  });

  factory OpnameLocation_Room.fromOpnameLocation(DataOpnameLocation location) =>
      OpnameLocation_Room(
          roomSiteID: location.siteId,
          roomId: location.siteRoomId,
          roomName: location.siteRoomName,
          statusOpname: location.statusOpname);
}
