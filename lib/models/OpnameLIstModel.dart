// To parse this JSON data, do
//
//     final opnameListModel = opnameListModelFromJson(jsonString);

import 'dart:convert';

OpnameListModel opnameListModelFromJson(String str) =>
    OpnameListModel.fromJson(json.decode(str));

String opnameListModelToJson(OpnameListModel data) =>
    json.encode(data.toJson());

class OpnameListModel {
  String result;
  List<ListOpname> data;

  OpnameListModel({
    this.result,
    this.data,
  });

  factory OpnameListModel.fromJson(Map<String, dynamic> json) =>
      OpnameListModel(
        result: json["result"],
        data: List<ListOpname>.from(
            json["data"].map((x) => ListOpname.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class ListOpname {
  String barcode;
  String assetname;
  int headeropnameid;
  dynamic opnameDate;
  String statusopname;

  ListOpname({
    this.barcode,
    this.assetname,
    this.headeropnameid,
    this.opnameDate,
    this.statusopname,
  });

  factory ListOpname.fromJson(Map<String, dynamic> json) => ListOpname(
        barcode: json["barcode"],
        assetname: json["assetname"],
        headeropnameid: json["headeropnameid"],
        opnameDate: json["OpnameDate"],
        statusopname: json["statusopname"],
      );

  Map<String, dynamic> toJson() => {
        "barcode": barcode,
        "assetname": assetname,
        "headeropnameid": headeropnameid,
        "OpnameDate": opnameDate,
        "statusopname": statusopname,
      };
}
