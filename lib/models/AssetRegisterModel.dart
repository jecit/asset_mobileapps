// To parse this JSON data, do
//
//     final assetRegister = assetRegisterFromJson(jsonString);

import 'dart:convert';

AssetRegister assetRegisterFromJson(String str) =>
    AssetRegister.fromJson(json.decode(str));

// String assetRegisterToJson(AssetRegister data) => json.encode(data.toJson());

class AssetRegister {
  String assetcode;
  String barcode;
  String assetname;
  String assetcat1;
  String assetcat2;
  String assetcat3;
  String assetroom;
  String assetsite;
  String assetpt;
  String image1;
  String image2;
  String image3;
  String imagepath;

  AssetRegister(
      {this.assetcode,
      this.barcode,
      this.assetname,
      this.assetcat1,
      this.assetcat2,
      this.assetcat3,
      this.assetroom,
      this.assetsite,
      this.assetpt,
      this.image1,
      this.image2,
      this.image3,
      this.imagepath});

  factory AssetRegister.fromJson(Map<String, dynamic> json) =>
      new AssetRegister(
        assetcode: json["assetcode"],
        barcode: json["barcode"],
        assetname: json["assetname"],
        assetcat1: json["assetcat1"],
        assetcat2: json["assetcat2"],
        assetcat3: json["assetcat3"],
        assetroom: json["assetroom"],
        assetsite: json["assetsite"],
        assetpt: json["assetpt"],
        image1: json["image1"],
        image2: json["image2"],
        image3: json["image3"],
        imagepath: json["imagepath"],
      );

  Map<String, dynamic> toJson(String sImg1, String sImg2, String sImg3) => {
        "assetcode": assetcode,
        "barcode": barcode,
        "assetname": assetname,
        "assetcat1": assetcat1,
        "assetcat2": assetcat2,
        "assetcat3": assetcat3,
        "assetroom": assetroom,
        "assetsite": assetsite,
        "assetpt": assetpt,
        "image1": sImg1,
        "image2": sImg2,
        "image3": sImg3,
        "imagepath": imagepath,
      };

  Map<String, dynamic> toJson2() => {
        "assetcode": assetcode,
        "assetname": assetname,
        "assetcat1": assetcat1,
        "assetcat2": assetcat2,
        "assetcat3": assetcat3,
        "assetroom": assetroom,
        "assetsite": assetsite,
        "assetpt": assetpt,
      };

  Map<String, dynamic> toJsonImage(String sImg1, String sImg2, String sImg3) =>
      {
        "barcode": barcode,
        "image1": sImg1,
        "image2": sImg2,
        "image3": sImg3,
        "imagepath": imagepath,
      };
}
